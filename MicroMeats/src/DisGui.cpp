#include "DisGui.h"

bool show_test_window;

DisGui::DisGui()
{
	SetupGui();
}

//https://github.com/ocornut/imgui/blob/master/docs/FONTS.md

void DisGui::setup_font()
{
	ImGuiIO& io = ImGui::GetIO();
	std::string fp = "/usr/share/fonts/truetype/cmu/cmunss.ttf";
	ofFile f(fp);
	
	if(f.doesFileExist(f.getAbsolutePath()))
	{
		//io.Fonts->AddFontFromFileTTF(fp.c_str(), 14);	
		io.Fonts->AddFontFromFileTTF(fp.c_str(), 24);	
		//io.Fonts->AddFontFromFileTTF(fp.c_str(), 32);	
	}
	
}

void DisGui::SetupGui()
{
	setup_font();

	//set window properties
	static bool no_titlebar = false;
	//static bool no_border = true;
	static bool no_resize = true;
	static bool no_move = true;
	static bool no_scrollbar = false;
	static bool no_collapse = true;
	static bool no_menu = true;
	static bool no_settings = true;
	//static float bg_alpha = -0.01f; // <0: default
	//bool show = true;

	// Demonstrate the various window flags.
	// Typically you would just use the default.
	ImGuiWindowFlags window_flags = 0;
	if (no_titlebar)
		window_flags |= ImGuiWindowFlags_NoTitleBar;
	//if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)
		window_flags |= ImGuiWindowFlags_NoResize;
	if (no_move)
		window_flags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar)
		window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)
		window_flags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)
		window_flags |= ImGuiWindowFlags_MenuBar;
	if (no_settings)
		window_flags |= ImGuiWindowFlags_NoSavedSettings;

}

void DisGui::GuiDisplay()
{
	// Display GUI
	begin();

	ImGui::Checkbox("Show Test Window", &show_test_window);
	if (show_test_window)
	{
		ImGui::ShowDemoWindow(&show_test_window);
	}

	ImGui::Spacing();

	ImGui::SliderInt("noise_octaves_octaves", &mm.noise_octaves_octaves, 0, 50);//ofGetWindowWidth()/2);
	ImGui::SliderFloat("noise_octaves_persistence", &mm.noise_octaves_persistence, 0, 1);
	ImGui::SliderFloat("noise_octaves_frequency", &mm.noise_octaves_frequency, 0, 100);
	ImGui::SliderFloat("noise_octaves_amplitude",&mm.noise_octaves_amplitude, 0, 100);
	
	
	ImGui::SliderFloat("sphere_sphere_radius",&mm.sphere_sphere_radius, 0, 800);
	ImGui::SliderFloat("sphere_c",&mm.sphere_c, 0, 1000);
	ImGui::SliderInt("sphere_num_points",&mm.sphere_num_points,0,1000);
	float sphere_sphere_radius;
  	float sphere_c;
  	unsigned int sphere_num_points;
    
	//ImGui::ShowFontSelector("font");
    
	
	end();
}
