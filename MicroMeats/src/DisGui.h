#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"
#include "MicroMeat.h"

class DisGui : public ofxImGui::Gui
{

public:
  // Generic Gui Components
  void GuiDisplay(void); //For organization purposes
  void SetupGui(void);   //initial setup parameters

  bool DIAGNOSTICS = TRUE;
  
  DisGui();

  //variables for this GUI
  MicroMeat mm;
  
  
  void setup_font(void);

};
