#include "ofMain.h"

class MicroMeat : public ofPolyline
{
	public:

	MicroMeat();
    ofPolyline create_spiral_planet(ofPoint origin, float sphere_radius, float c, unsigned int num_points, float finished_percent);

    void iter_01();
	void iter_02();
	void iter_03();
	void iter_04();
	void iter_05();
	void iter_06();
	void iter_07();

	//params for octave noise
	int noise_octaves_octaves;
    float noise_octaves_persistence;
    float noise_octaves_frequency;
	float noise_octaves_amplitude;

	//parameters for the planet
	float sphere_sphere_radius;
  	float sphere_c;
  	int sphere_num_points;
  
    float noise_scaler;

    
	float octavePerlin(float x, float y, float z, int octaves, float persistence);

	
};
