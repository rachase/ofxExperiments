#include "MicroMeat.h"

MicroMeat::MicroMeat()
{

}
//https://forum.openframeworks.cc/t/noisedetail-in-openframeworks/33164/2

void MicroMeat::iter_01()
{
    //full spiral planet

    float sphere_radius = ofGetWindowWidth() / 4;
    float c = 200;
    unsigned int points_per_circle = 140;
    unsigned int total_points = points_per_circle * c;
    float finished_percent = 1;
    ofPoint origin;
    origin.x = ofGetWindowWidth() / 2;
    origin.y = ofGetWindowHeight() / 2;
    origin.z = 0;

    //var random_sampler = new OctaveSimplex(3, 1, 0.1);

    ofPolyline polygons = create_spiral_planet(origin, sphere_radius, c, total_points, finished_percent);

    polygons.draw();
}

void MicroMeat::iter_02()
{
}

void MicroMeat::iter_03()
{
}

void MicroMeat::iter_04()
{
}

void MicroMeat::iter_05()
{
}

void MicroMeat::iter_06()
{
}

void MicroMeat::iter_07()
{
}

float MicroMeat::octavePerlin(float x, float y, float z, int octaves, float persistence)
{
    float total = 0;
    // float frequency = 1;
    // float amplitude = .01;
    float maxValue = 0;

    octaves = noise_octaves_octaves;
    persistence = noise_octaves_persistence;
    float frequency = noise_octaves_frequency;
    float amplitude = noise_octaves_amplitude;
    
    for (int i = 0; i < octaves; i++)
    {
        total += ofNoise(x * frequency, y * frequency, z * frequency) * amplitude;
        maxValue += amplitude;
        amplitude *= persistence;
        frequency *= 2;
    }
    return total / maxValue;
}

ofPolyline MicroMeat::create_spiral_planet(ofPoint origin, float sphere_radius, float c, unsigned int num_points,
    float finished_percent)
{
  
  ofPolyline planet;

  sphere_radius=sphere_sphere_radius;
  c=sphere_c;
  num_points=sphere_num_points;

  float theta_inc = M_PI / float(num_points);

  for (int i = 0; i < (num_points * finished_percent); i++)
  {
    float theta = i * theta_inc;
    float phi = c * theta;
    float x = origin.x + sphere_radius * sin(theta) * cos(phi);
    float y = origin.y + sphere_radius * sin(theta) * sin(phi);
    float z = origin.z + sphere_radius * cos(theta);
    //float noise = ofNoise(x,y,z);
    float noise = octavePerlin(x,y,z,1,1);
    //var noise = random_sampler.sample3D(x, y, z);
    float noise_radius = sphere_radius * noise;
    x = origin.x + noise_radius * sin(theta) * cos(phi);
    y = origin.y + noise_radius * sin(theta) * sin(phi);
    z = origin.z + noise_radius * cos(theta);
    planet.curveTo(ofPoint(x,y,z));
  }

  return planet;
}

