#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
  ofEnableDepthTest();

  //ofSetVerticalSync(true);
  ofSetFrameRate(60);
  ofEnableDepthTest();
  ofEnableAlphaBlending();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  gui.setup();
}

//--------------------------------------------------------------

void ofApp::update()
{
}

void ofApp::DisplayDiagnostics()
{
  string fpsStr;
  int base_x = 100, base_y = 100;
  int space = 12;
  int i = 0;

  base_x = ofGetWindowWidth() - 260;
  base_y = ofGetWindowHeight() - 75;
  ofSetColor(0, 0, 0, 255);
  i++;

  //frame rate Statistics
  fpsStr = "frame rate/target: " + ofToString(ofGetFrameRate(), 2) + '/' + ofToString(ofGetTargetFrameRate(), 2);

  ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
  i++;

  fpsStr = "frame num: " + ofToString(ofGetFrameNum(), 2);
  ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
  i++;

  fpsStr = "last frame time: " + ofToString(ofGetLastFrameTime(), 2);
  ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
  i++;

  //Window Statistics
  fpsStr = "window h/w: " + ofToString(ofGetWindowHeight(), 2) + '/' + ofToString(ofGetWindowWidth(), 2);
  ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
  i++;

  fpsStr = "window pos x/y: " + ofToString(ofGetWindowPositionX(), 2) + '/' + ofToString(ofGetWindowPositionY(), 2);
  ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
  i++;
}

void ofApp::draw()
{
  ofBackground(255,255,255,0);
  gui.draw();

  //copy over the params for octave noise
  mm=gui.mm;
  

  mm.iter_01();
  if (gui.DIAGNOSTICS)
    DisplayDiagnostics();

  gui.GuiDisplay();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
//This function gets called when the any mouse button is pushed down
void ofApp::mousePressed(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
}
