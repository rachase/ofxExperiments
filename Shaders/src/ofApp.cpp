#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  filesystem::path path = filesystem::path("../../data");
  shader.load("../../data/shader");
  //shader.load(path/"shader.vert",path/"shader.frag");

}

//---------------------------------------------------------
void ofApp::update()
{
 shader.begin();
 shader.setUniform1f("u_red",sin(ofGetElapsedTimef())); 
 shader.end();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    shader.begin();
    ofDrawRectangle(10,10,ofGetWidth(),100);
    shader.end();
}