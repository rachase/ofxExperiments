#pragma once

#include "ofMain.h"
#include "ofxShader.h"
class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
    
    ofxShader shader;
  };

