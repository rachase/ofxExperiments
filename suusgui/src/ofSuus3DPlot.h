#pragma once
#include "ofMain.h"
#include <vector>
class ofSuus3DPlot
{

  public:

    ofSuus3DPlot(void);

    double limit_x_upper,limit_y_upper,limit_z_upper;
    double limit_x_lower,limit_y_lower,limit_z_lower;

    double steps_x,steps_y,steps_z;
    double stepsize_x,stepsize_y,stepsize_z;

    bool render_xplane,render_yplane,render_zplane;

    ofVec3f position;

    void draw();
    void drawgrid(float stepsize,float number_of_steps,float scale);


};
