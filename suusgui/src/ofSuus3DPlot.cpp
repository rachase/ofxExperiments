#include "ofSuus3DPlot.h"

ofSuus3DPlot::ofSuus3DPlot(void)
{
  //double limit_x_upper,limit_y_upper,limit_z_upper;
  //double limit_x_lower,limit_y_lower,limit_z_lower;

  steps_x=10;
  steps_y=5;
  steps_z=20;

  stepsize_x=10;
  stepsize_y=20;
  stepsize_z=5;

  render_xplane=true;
  render_yplane=true;
  render_zplane=true;
}


void ofSuus3DPlot::drawgrid(float stepsize,float number_of_steps,float scale)
{
  for (int iDimension=0; iDimension<2; iDimension++)
    {
      for (size_t i=0; i <= number_of_steps; i++)
      {
        float pos = i * stepsize;

        if (i == number_of_steps || i == 0)
          ofSetLineWidth(2);   // central axis or cap line
        else if ( i % 2 == 0){
          ofSetLineWidth(1.5); // major
        } else {
          ofSetLineWidth(1);   // minor
        }

        if (iDimension == 0 )
        {
          ofDrawLine(pos, 0, 0, pos, scale,0);
          if (pos !=0) ofDrawLine(0, -pos, -scale, 0, -pos, scale);
        }
        else
        {
          //renderer->drawLine(0, -scale, yz, 0, scale, yz);
          ofDrawLine(0, -scale,pos, 0, scale, pos);
          if (pos !=0)ofDrawLine(0, -scale,-pos, 0, scale, -pos);
        }
      }
    }
}

void ofSuus3DPlot::draw()
{
  ofSetColor(255,255,255,255);
  float scale_x,scale_y,scale_z;
  float pos;

  scale_x = stepsize_x * steps_x;
  scale_y = stepsize_y * steps_y;
  scale_z = stepsize_z * steps_z;

  //render the x lines on yz and zx planes;
  for (size_t i=0; i <= steps_x; i++)
  {
    pos = i * stepsize_x;
    if(render_yplane)ofDrawLine(pos, 0, 0, pos, scale_y, 0);
    if(render_zplane)ofDrawLine(pos, 0, 0, pos, 0, scale_z);
  }

  //render the y lines on xy and yz planes;
  for (size_t i=0; i <= steps_y; i++)
  {
    pos = i * stepsize_y;
    if(render_xplane)ofDrawLine(0, pos, 0, scale_x,pos,0);
    if(render_zplane)ofDrawLine(0, pos, 0, 0,pos,scale_z);
  }

  //render the y lines on xy and yz planes;
  for (size_t i=0; i <= steps_z; i++)
  {
    pos = i * stepsize_z;
    if(render_xplane)ofDrawLine(0, 0, pos, scale_x,0,pos);
    if(render_yplane)ofDrawLine(0, 0, pos, 0, scale_y,pos);
  }

/*
  stepsize = stepsize_y;
  number_of_steps = steps_y;
  scale = stepsize * number_of_steps;

  ofSetColor(255,0,0,255);

  drawgrid(stepsize,number_of_steps,scale);


  ofSetColor(255,0,255,255);

  stepsize = stepsize_z;
  number_of_steps = steps_z;
  scale = stepsize * number_of_steps;

  ofPushMatrix();
  ofRotateZDeg(90);
  drawgrid(stepsize,number_of_steps,scale);
  ofPopMatrix();
*/
}
