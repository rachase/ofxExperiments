#include "ofMain.h"
#include "ofApp.h"
#include <ros/ros.h>

//========================================================================
int main(int argc, char *argv[]){
	ros::init(argc, argv, "suus_ros_gui_publisher");

	#ifdef OF_TARGET_OPENGLES
		ofGLESWindowSettings settings;
		settings.glesVersion=2;
	#else
		ofGLWindowSettings settings;
		settings.setGLVersion(3,2);
	#endif

	settings.setSize(1920, 1080);
	ofCreateWindow(settings);

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new ofApp());

}
