#pragma once

#include "ofMain.h"
#include <iostream>
#include <sstream>
#include <string>
#include "ofSuusRos.h"
#include "ofSuusGUI.h"
#include "ofSuusCam.h"
#include "ofSuus25DGrid.h"
#include "ofSuus3DPlot.h"

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

    void DisplayDiagnostics(void);
		ofSuusRos suusros;
		ofSuusGUI suusgui;
		ofSuusCam suuscam;

		ofEasyCam camera[5];
		ofRectangle camera_view_box[5];

		ofSuus25DGrid channelmatrix;
		ofSuus3DPlot grid;

		std::vector<ofSuus25DGrid> adj_matrices_grids;

		ofShader shader;


};
