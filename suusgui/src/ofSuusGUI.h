#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"

class ofSuusGUI : public ofxImGui::Gui
{

  public:
  bool DIAGNOSTICS = TRUE;
  // Gui Components
  void GUIDisplay(void); 		//For organization purposes
  void SetupGUI(void);         //initial setup parameters

  bool cam_sliders = FALSE;  //overrides easy cam to use sliders
  float cam_position_x;
  float cam_position_y;
  float cam_position_z;
  float cam_rotation_x;
  float cam_rotation_y;
  float cam_rotation_z;
  float cam_target_x;
  float cam_target_y;
  float cam_target_z;

  int model_file = 0; //Id of current Model


  int adj_matrix_to_display = 0;


};
