#include "ofApp.h"
#include <ofMath.h>

ofMesh mesh;

//--------------------------------------------------------------
void ofApp::setup()
{
  //setup the suus ros interface
  suusros.setup();
  //setup the camera
  suuscam.setup();

  //setup the environment
  ofSetFrameRate(30);
  ofEnableDepthTest();
  ofEnableAlphaBlending();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  suusgui.setup();

  #ifdef TARGET_OPENGLES
  	shader.load("shadersES2/shader");
  #else
  	if(ofIsGLProgrammableRenderer()){
  		shader.load("shadersGL3/shader");
  	}else{
  		shader.load("shadersGL2/shader");
  	}
  #endif

  ofSuus25DGrid f;
  adj_matrices_grids.push_back(f);
  adj_matrices_grids.push_back(f);
  adj_matrices_grids.push_back(f);
  adj_matrices_grids.push_back(f);
  adj_matrices_grids.push_back(f);

  ofSetBackgroundColor(0,0,0,255);

  int height=30;
  int width = 60;
  for (int y = 0; y < height; y++) {
  for (int x = 0; x<width; x++){
    mesh.addVertex(ofPoint(x,y,sin(ofDegToRad(x*5))*20)); // make a new vertex
    mesh.addColor(ofColor(x,0,y));  // add a color at that vertex
  }
  }

  // now it's important to make sure that each vertex is correctly connected with the
  // other vertices around it. This is done using indices, which you can set up like so:
  for (int y = 0; y<height-1; y++){
  for (int x=0; x<width-1; x++){
    mesh.addIndex(x+y*width);               // 0
    mesh.addIndex((x+1)+y*width);           // 1
    mesh.addIndex(x+(y+1)*width);           // 10

    mesh.addIndex((x+1)+y*width);           // 1
    mesh.addIndex((x+1)+(y+1)*width);       // 11
    mesh.addIndex(x+(y+1)*width);           // 10
  }
  }

  camera_view_box[0].set(310, 0, 1000, 920);
  camera_view_box[1].set(0, 0, 300, 300);
  camera_view_box[2].set(0, 310, 300, 300);
  camera_view_box[3].set(0, 620, 300, 300);
  camera_view_box[4].set(1310, 0, 600, 920);

}

//--------------------------------------------------------------
void ofApp::update()
{
  ros::spinOnce();
  int r,c;
  for(int i=0;i<adj_matrices_grids.size();i++)
  {
    r = suusros.multiarrays.at(i).layout.dim.at(0).stride;
    c = suusros.multiarrays.at(i).layout.dim.at(0).size;
    r = r/c;
    adj_matrices_grids.at(i).rows = r;
    adj_matrices_grids.at(i).cols = c;
    adj_matrices_grids.at(i).heightdata = suusros.multiarrays.at(i).data;
    adj_matrices_grids.at(i).set();
  }
}


//--------------------------------------------------------------
void ofApp::draw()
{
  /*
  suuscam.begin();
  //draw here.


  ofSetColor(255*((float)suusgui.adj_matrix_to_display/adj_matrices_grids.size()),0,128,255);
  ofTranslate(50,0,0);
  adj_matrices_grids.at(suusgui.adj_matrix_to_display).draw();
  ofTranslate(-50,0,0);

  suuscam.end();
*/


  camera[0].begin(camera_view_box[0]);
  shader.begin();
    grid.draw();
    mesh.drawWireframe();
  shader.end();
  camera[0].end();
  ofNoFill();
  ofSetLineWidth(1);         // set line width to 10
  ofSetColor(255,255,255,255);
  ofDrawRectangle(camera_view_box[0]);
  camera[1].begin(camera_view_box[1]);
    adj_matrices_grids.at(suusgui.adj_matrix_to_display).draw();
  camera[1].end();
  ofNoFill();
  ofSetColor(255,255,255,255);
  ofDrawRectangle(camera_view_box[1]);
  camera[2].begin(camera_view_box[2]);
    adj_matrices_grids.at(suusgui.adj_matrix_to_display).draw();
  camera[2].end();
  ofNoFill();
  ofSetColor(255,255,255,255);
  ofSetLineWidth(10);         // set line width to 10
  ofDrawRectangle(camera_view_box[2]);
  camera[3].begin(camera_view_box[3]);
    adj_matrices_grids.at(suusgui.adj_matrix_to_display).draw();
  camera[3].end();
  ofNoFill();
  ofSetColor(255,255,255,255);
  ofDrawRectangle(camera_view_box[3]);
  ofDrawRectangle(camera_view_box[4]);

  if (suusgui.DIAGNOSTICS)DisplayDiagnostics();
  suusgui.GUIDisplay();
  //suusros.MessageDump();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

  //we are on IMGUI, disable other mouse inputs, enable again in MouseReleased
  if (ImGui::GetIO().WantCaptureMouse)
  {
    suuscam.disableMouseInput();        //disable the camera movement
    for(int i=0;i<5;i++)
    {
      camera[i].disableMouseInput();
    }
  }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
  suuscam.enableMouseInput();
  for(int i=0;i<5;i++)
  {
    camera[i].enableMouseInput();
  }
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::DisplayDiagnostics()
{
  string fpsStr;
  int base_x=100,base_y=100;
  int space=12;
  int i=0;

  base_x = ofGetWindowWidth()-260;
  base_y = ofGetWindowHeight()-75;
  ofSetColor(255,255,255,255);
  i++;

  //frame rate Statistics
  fpsStr = "frame rate/target: "
          +ofToString(ofGetFrameRate(), 2) + '/'
          +ofToString(ofGetTargetFrameRate(), 2);

  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "frame num: "+ofToString(ofGetFrameNum(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "last frame time: "+ofToString(ofGetLastFrameTime(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  //Window Statistics
  fpsStr = "window h/w: " +ofToString(ofGetWindowHeight(), 2) + '/'
                          +ofToString(ofGetWindowWidth(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "window pos x/y: " +ofToString(ofGetWindowPositionX(), 2) + '/'
                              +ofToString(ofGetWindowPositionY(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

}

/*
   //test shader
  float percentX = 1000 / (float)ofGetWidth();
  percentX = ofClamp(percentX, 0, 1);

  // the mouse/touch X position changes the color of the plane.
  // please have a look inside the frag shader,
  // we are using the globalColor value that OF passes into the shader everytime you call ofSetColor().
  ofColor colorLeft = ofColor::magenta;
  ofColor colorRight = ofColor::cyan;
  ofColor colorMix = colorLeft.getLerped(colorRight, percentX);
  ofSetColor(colorMix);

  shader.begin(); // start shading!
  // a lot of the time you have to pass in variables into the shader.
  // in this case we need to pass it the elapsed time for the sine wave animation.
  shader.setUniform1f("time", ofGetElapsedTimef());

  // translate plane into center screen.
  float tx = ofGetWidth() / 2;
  float ty = ofGetHeight() / 2;
  //ofTranslate(tx, ty);

  ofDrawBox(2,2,2,40,5,6);
  ofTranslate(15, 15, 15);
  channelmatrix.draw();

  shader.end(); // end shading!
*/
