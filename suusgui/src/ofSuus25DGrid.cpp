#include "ofSuus25DGrid.h"

//constructor
ofSuus25DGrid::ofSuus25DGrid(void)
{
  setup(4,4);
}

void ofSuus25DGrid::setup(int numrows, int numcols)
{
  if ((numrows<0)||(numcols<0))
  {
    numrows = 1;
    numcols = 1;
  }
  heightdata.clear();
  cells.clear();
  rows=numrows;
  cols=numcols;
  cellwidth=4;
  celllength=4;
  cellseperation=1;
  limit_upper=30;
  limit_lower=0;
  ofBoxPrimitive B;
  cells.clear();

  for(int i=0;i<rows;i++)
  {
    for(int j=0;j<cols;j++)
    {
      //set Width,Height,Depth
      //B.set(cellwidth,celllength,data.at(i*datawidth+j));
      B.set(cellwidth,celllength,(i+1)*(j+1));
      B.setResolution(10,10,10);
      B.setMode(OF_PRIMITIVE_TRIANGLES);
      B.setPosition(i*(cellwidth+cellseperation), j*(celllength+cellseperation),0);
      cells.push_back(B);
    }
  }

}

void ofSuus25DGrid::set()
{
  //sanity check
  if(heightdata.size()!=cells.size())
  {
    //std::cout << "height " << heightdata.size() << " cell " << cells.size() <<std::endl;
    //std::cout << "rows  " << rows << " cols " << cols <<std::endl;
    setup(rows,cols);
  }

  //update heights
  for(int i=0;i<heightdata.size();i++)
  {
    cells.at(i).setDepth(heightdata.at(i));
  }

  //applylimits();

  for(int i=0;i<heightdata.size();i++)
  {
    cells.at(i).setPosition( cells.at(i).getX(),
                             cells.at(i).getY(),
                             cells.at(i).getDepth()/2);
  }
}

void ofSuus25DGrid::randomize()
{
  ofVec3f temp;
  for(int i=0;i<cells.size();i++)
  {
    cells.at(i).setDepth(cells.at(i).getDepth()+ofRandomf()*.375);
    cells.at(i).setPosition( cells.at(i).getX(),
                             cells.at(i).getY(),
                             cells.at(i).getDepth()/2);
    cells.at(i).setResolution(10,10,cells.at(i).getDepth());
  }
}

void ofSuus25DGrid::applylimits()
{
  for(int i=0;i<cells.size();i++)
  {
    if(cells.at(i).getDepth()>limit_upper)
    {
      cells.at(i).setDepth(limit_upper);
    }
    if(cells.at(i).getDepth()<limit_lower)
    {
      cells.at(i).setDepth(limit_lower);
    }
  }
}

void ofSuus25DGrid::draw()
{
   glm::vec3 o,p,j;
  for(int i=0;i<cells.size();i++)
  {
    ofSetColor(0,255/5,128/5,255);
    cells.at(i).draw();
    o = cells.at(i).getPosition();
    p = cells.at(i).getSize();
    o -= p/2;
    j = o + p;
    ofSetColor(255,255,255,255);
    ofDrawLine(glm::vec3(o.x,o.y,o.z),glm::vec3(o.x+p.x,o.y,o.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y,o.z),glm::vec3(o.x+p.x,o.y+p.y,o.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y+p.y,o.z),glm::vec3(o.x,o.y+p.y,o.z));
    ofDrawLine(glm::vec3(o.x,o.y+p.y,o.z),glm::vec3(o.x,o.y,o.z));

    ofDrawLine(glm::vec3(o.x,o.y,p.z),glm::vec3(o.x+p.x,o.y,p.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y,p.z),glm::vec3(o.x+p.x,o.y+p.y,p.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y+p.y,p.z),glm::vec3(o.x,o.y+p.y,p.z));
    ofDrawLine(glm::vec3(o.x,o.y+p.y,p.z),glm::vec3(o.x,o.y,p.z));

    ofDrawLine(glm::vec3(o.x,o.y,o.z),glm::vec3(o.x,o.y,o.z+p.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y,o.z),glm::vec3(o.x+p.x,o.y,o.z+p.z));
    ofDrawLine(glm::vec3(o.x+p.x,o.y+p.y,o.z),glm::vec3(o.x+p.x,o.y+p.y,o.z+p.z));
    ofDrawLine(glm::vec3(o.x,o.y+p.y,o.z),glm::vec3(o.x,o.y+p.y,o.z+p.z));

  }
}
