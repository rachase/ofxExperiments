#pragma once
#include "ofMain.h"
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64MultiArray.h>
#include <boost/algorithm/string.hpp>
#include "ofSuus25DGrid.h"

class ofSuusRos
{

  public:

    int argc;
    char **argv;

    ros::NodeHandle n;
    //ros::Publisher chatter_pub;
    ros::Subscriber sub_adj_distance,
                    sub_adj_power_received,
                    sub_adj_snr,
                    sub_adj_channel_capacity,
                    sub_positionlist;

    void distance_callback(const std_msgs::Float64MultiArray::ConstPtr& val);
    void power_callback(const std_msgs::Float64MultiArray::ConstPtr& val);
    void snr_callback(const std_msgs::Float64MultiArray::ConstPtr& val);
    void channel_callback(const std_msgs::Float64MultiArray::ConstPtr& val);
    void positionlist_callback(const std_msgs::Float64MultiArray::ConstPtr& val);

    //copy of last messages received so main app can access data
    std::vector<std_msgs::Float64MultiArray> multiarrays;
    std::vector<std::string> adj_matrices_names;

    std::vector<std::string> namelist;

    //values for rf environment
    double FrequencyInMhz;
    double BWInMhz;
    double NoiseFloorDefaultDB;

    //values for sim environment
    bool fakedroneposes;
    double randomwalkmps;
    double randomwalkdeltaz;
    double minlat;
    double minlon;
    double maxlat;
    double maxlon;
    double maxalt;
    double minalt;

    bool load_parameters();
    void setup(void);

    void MessageDump(void);
};
