#include "ofSuusRos.h"

const char * adj_matrices_names[] {
  "Distance (m)",
  "Power (dB)",
  "SNR",
  "Channel (kbps)",
  "Positions"
};

void ofSuusRos::distance_callback(const std_msgs::Float64MultiArray::ConstPtr& val)
{
  multiarrays.at(0).layout = val->layout;
  multiarrays.at(0).data = val->data;
}

void ofSuusRos::power_callback(const std_msgs::Float64MultiArray::ConstPtr& val)
{
  multiarrays.at(1).layout = val->layout;
  multiarrays.at(1).data = val->data;
}

void ofSuusRos::snr_callback(const std_msgs::Float64MultiArray::ConstPtr& val)
{
  multiarrays.at(2).layout = val->layout;
  multiarrays.at(2).data = val->data;
}

void ofSuusRos::channel_callback(const std_msgs::Float64MultiArray::ConstPtr& val)
{
  multiarrays.at(3).layout = val->layout;
  multiarrays.at(3).data = val->data;
}

void ofSuusRos::positionlist_callback(const std_msgs::Float64MultiArray::ConstPtr& val)
{
  multiarrays.at(4).layout = val->layout;
  multiarrays.at(4).data = val->data;
}

void ofSuusRos::setup()
{
  std::string s;

  //set up multiarray vector
  std_msgs::Float64MultiArray f;
  std::vector<double> d;
  std_msgs::MultiArrayDimension dim;

  dim.size = 16;
  dim.stride = 4;
  dim.label = "none";
  f.layout.dim.push_back(dim);
  for(int i=0;i<16;i++)d.push_back(i);
  f.data=d;

  multiarrays.push_back(f);
  multiarrays.push_back(f);
  multiarrays.push_back(f);
  multiarrays.push_back(f);
  multiarrays.push_back(f);

  //setup publishers and subscriptions
  sub_adj_distance = n.subscribe("rf/adj_distance",1, &ofSuusRos::distance_callback, this);
  sub_adj_power_received = n.subscribe("rf/adj_power_received",1, &ofSuusRos::power_callback, this);
  sub_adj_snr = n.subscribe("rf/adj_snr",1, &ofSuusRos::snr_callback, this);
  sub_adj_channel_capacity = n.subscribe("rf/adj_channel_capacity",1, &ofSuusRos::channel_callback, this);
  sub_positionlist = n.subscribe("rf/positionlist",1, &ofSuusRos::positionlist_callback, this);

  //load all the parameters from the ros parameters server
  load_parameters();
}


bool ofSuusRos::load_parameters()
{
  bool oktoboot=false;
  if(n.getParam("oktoboot",oktoboot));
  if(!oktoboot) return false;

  //get all the rf parameters
  n.getParam("rf/FrequencyInMhz",FrequencyInMhz);
  n.getParam("rf/BWInMhz",BWInMhz);
  n.getParam("rf/NoiseFloorDefaultDB",NoiseFloorDefaultDB);

  //get the sim parameters
  n.getParam("sim/fakedroneposes",fakedroneposes);
  n.getParam("sim/randomwalkmps",randomwalkmps);
  n.getParam("sim/randomwalkdeltaz",randomwalkdeltaz);

  n.getParam("sim/minlat",minlat);
  n.getParam("sim/minlon",minlon);
  n.getParam("sim/maxlat",maxlat);
  n.getParam("sim/maxlon",maxlon);
  n.getParam("sim/maxalt",maxalt);
  n.getParam("sim/minalt",minalt);

  //get the name list, split it.
  std::string temp;
  n.getParam("sim/namelist",temp);
  boost::split(namelist,temp,boost::is_any_of(","));

  return true;

}

/*
void ofSuusRos::MessageDump()
{
  string Str;
  int base_x=100,base_y=100;
  int space=12;
  int i=0;

  base_x = ofGetWindowWidth()-260;
  base_y = ofGetWindowHeight()-75;
  ofSetColor(255,255,255,255);
  i++;

  base_x =30;
  base_y =30;
  //frame rate Statistics
  Str = "Message 1 ";
  ofDrawBitmapString(Str, base_x,base_y+space*i);
  i++;

  Str = "Message 2 ";
  ofDrawBitmapString(Str, base_x,base_y+space*i);
  i++;

  Str = "Message 3 ";
  ofDrawBitmapString(Str, base_x,base_y+space*i);
  i++;

  //Window Statistics
  Str = "Message 4 ";

  ofDrawBitmapString(Str, base_x,base_y+space*i);
  i++;

  Str = "Message 5 ";

  ofDrawBitmapString(Str, base_x,base_y+space*i);
  i++;

}
*/
