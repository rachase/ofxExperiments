#pragma once
#include "ofMain.h"
#include <vector>
class ofSuus25DGrid
{

  public:

    ofSuus25DGrid(void);

    void setup(int length, int width);
    void set();
    bool doesdatamatch(int length, int width, int size);
    void randomize(void);
    void applylimits(void);

    void draw();

    int rows;  //number of rows(or cols)
    int cols;   //number of cols(or rows)

    //graph parameters
    double limit_upper;  //upper height of bar
    double limit_lower;  //lower height of bar

    //display parameters
    double cellwidth;
    double celllength;
    double cellseperation;

    std::vector<ofBoxPrimitive> cells;

    //data
    std::vector<double> heightdata;

    //other
    bool normalize;     //normalize in real time?

};
