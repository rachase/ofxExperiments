#include "ofSuusGUI.h"

const char * adj_matrices_list[] {
  "Distance (m)",
  "Power (dB)",
  "SNR",
  "Channel (kbps)",
  "Positions"
};

bool show_test_window;

void ofSuusGUI::SetupGUI()
{
  //set window properties
  static bool no_titlebar = false;
  static bool no_border = true;
  static bool no_resize = true;
  static bool no_move = true;
  static bool no_scrollbar = false;
  static bool no_collapse = true;
  static bool no_menu = true;
  static bool no_settings = true;
  static float bg_alpha = -0.01f; // <0: default
  bool show = true;

  int myWidth = 10;
  int myHeight = 20;
  // Demonstrate the various window flags.
  // Typically you would just use the default.
  ImGuiWindowFlags window_flags = 0;
  if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
  //if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
  if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
  if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
  if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
  if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;
  if (!no_menu)     window_flags |= ImGuiWindowFlags_MenuBar;
  if (no_settings) window_flags |= ImGuiWindowFlags_NoSavedSettings;

  ImGui::Begin("My Title", &show, window_flags);
}


void ofSuusGUI::GUIDisplay()
{
	// Display GUI
  begin();

	ImGui::Spacing();
	ImGui::Checkbox("Show Test Window", &show_test_window);
	if(show_test_window)
	{
			ImGui::ShowDemoWindow(&show_test_window);
	}

	//Dropdown for names of adj matrices
	ImGui::Combo("Model", &adj_matrix_to_display, adj_matrices_list, 5);

  end();
}
