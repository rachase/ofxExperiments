#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxAssimpModelLoader.h"
#include "ofxImGui.h"
#include "ant_node.h"
#include "MdsGui.h"
#include "MdsCam.h"
#include "operationalarea.h"


#define MAX_NODES 1024
#define X_SIZE 500
#define Y_SIZE 500
#define Z_SIZE 500

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void DisplayIntersections();
		void DisplayDiagnostics();
		void setLightOri(ofLight &light, ofVec3f rot);

		//instance of the gui
		MdsGui gui;

		//instance of the camera
		MdsCam cam;
		operationalarea myoperationalarea;
		//vars for node Setup
		int num_centroids = 20;
		ofAntNode centroids[MAX_NODES];



};
