#include "ofApp.h"

ofxAssimpModelLoader model;

ofFloatColor link_color= { 1.0f,0.0f,0.25f,0.5f };
ofFloatColor model_color= { 0.0f,1.0f,0.50f,1.0f };

std::vector<ofVec3f> points;
ofConePrimitive cone,cone2,cone3,cone4;
ofMesh planet,planet2,planet3,planet4;

void ofApp::setLightOri(ofLight &light, ofVec3f rot)
{
    ofVec3f xax(1, 0, 0);
    ofVec3f yax(0, 1, 0);
    ofVec3f zax(0, 0, 1);
    ofQuaternion q;
    q.makeRotate(rot.x, xax, rot.y, yax, rot.z, zax);
    light.setOrientation(q);
}

/*
void ofApp::GUIDisplay()
{
  // Display GUI
  gui.begin();

	ImGui::Spacing();
	if(ImGui::CollapsingHeader("Links"))
	{
		ImGui::ColorEdit4("Color", (float *)&link_color);
		ImGui::Checkbox("Node Links", &drawlinks);
	}


	if(ImGui::CollapsingHeader("Nodes"))
	{
		ImGui::SliderInt("Node Count", &node_count, 0, 256);
		ImGui::SliderFloat("Model Scale", &sphere_radius_global, 0.0f, 50.0f);
		if(ImGui::Button("Reset Nodes"))
		{
				resetnodes = true;
		}
	}

	if(ImGui::CollapsingHeader("Node Models"))
	{
		ImGui::ColorEdit4("Color", (float *)&model_color);
		ImGui::Checkbox("FACES", &FACES);
		ImGui::SameLine();
		ImGui::Checkbox("EDGES", &EDGES);
		ImGui::SameLine();
		ImGui::Checkbox("POINTS", &POINTS);

		ImGui::Combo("Model", &model_file, MODEL_NAMES, 8);
		ImGui::SliderFloat("Model Scale", &model_scale, 0.0f, 1.0f);
	}

	if(ImGui::CollapsingHeader("Ground Model"))
	{
		ImGui::Text("TBD");
	}


  gui.end();
}
*/

//--------------------------------------------------------------
void ofApp::setup()
{

//  ofEnableDepthTest();

  //ofSetVerticalSync(true);
  ofSetFrameRate(60);
  ofEnableDepthTest();
  ofEnableAlphaBlending();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  //setup the camera
	cam.setup();

  //initialize centroid positions
  //NOTE: Make this a function of data class later
  for(int i=0;i<MAX_NODES;i++)
  {
    centroids[i].populate(gui.boundary);
  }

  //setup the GUI
  gui.setup();

	myoperationalarea.load_model();
  myoperationalarea.setup();

  cone.set( 30, 60, 20, 20 );
  cone2.set( 25, 50, 25, 25 );
  cone3.set( 20, 40, 30, 30 );
  cone4.set( 15, 30, 40, 40 );


  cone.setPosition(0,0,0);
  cone2.setPosition(0,5,0);
  cone3.setPosition(0,10,0);
  cone4.setPosition(0,15,0);


  planet = cone.getMesh();
  planet2 = cone2.getMesh();
  planet3 = cone3.getMesh();
  planet4 = cone4.getMesh();

  ofVec3f p;

  for (int i =0 ; i< 50 ; i++)
  {
    for (int j = 0 ; j < 50 ; j++)
    {
      p.x = 0 ;
      p.y =0 ;
      p.x =0 ;
    }
  }
  points.push_back(p);
}

//--------------------------------------------------------------
void ofApp::update(){

  static int current_model = -1;
  //reset all the node positions
  if(gui.resetnodes)
  {
    gui.resetnodes = false;
    for(int i=0;i<MAX_NODES;i++)
    {
      centroids[i].populate(gui.boundary);
    }
  }

	//change the number of centroids from gui slider
	num_centroids = gui.node_count;

	//use sliders if checkbox checked.
	if (gui.cam_sliders)
	{
		cam.setPosition(gui.cam_position_x,gui.cam_position_y,gui.cam_position_z);
		cam.setOrientation(ofVec3f(gui.cam_rotation_x,gui.cam_rotation_y,gui.cam_rotation_z));
		//cam.setTarget(ofVec3f(gui.cam_target_x,gui.cam_target_y,gui.cam_target_z));
	}

}


void ofApp::DisplayDiagnostics()
{
  string fpsStr;
  int base_x=100,base_y=100;
  int space=12;
  int i=0;

  base_x = ofGetWindowWidth()-260;
  base_y = ofGetWindowHeight()-75;
  ofSetColor(255,255,255,255);
  i++;

  //frame rate Statistics
  fpsStr = "frame rate/target: "
          +ofToString(ofGetFrameRate(), 2) + '/'
          +ofToString(ofGetTargetFrameRate(), 2);

  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "frame num: "+ofToString(ofGetFrameNum(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "last frame time: "+ofToString(ofGetLastFrameTime(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  //Window Statistics
  fpsStr = "window h/w: " +ofToString(ofGetWindowHeight(), 2) + '/'
                          +ofToString(ofGetWindowWidth(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "window pos x/y: " +ofToString(ofGetWindowPositionX(), 2) + '/'
                              +ofToString(ofGetWindowPositionY(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

}

void ofApp::DisplayIntersections()
{
  string fpsStr;
  int base_x=100,base_y=00;
  int space=12;
  int i=0;

  base_x = ofGetWindowWidth()-260;
  base_y = 0;
  ofSetColor(255,255,255,255);
  i++;

  //frame rate Statistics
  fpsStr = "frame rate/target: "
          +ofToString(ofGetFrameRate(), 2) + '/'
          +ofToString(ofGetTargetFrameRate(), 2);

  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "frame num: "+ofToString(ofGetFrameNum(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "last frame time: "+ofToString(ofGetLastFrameTime(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  //Window Statistics
  fpsStr = "window h/w: " +ofToString(ofGetWindowHeight(), 2) + '/'
                          +ofToString(ofGetWindowWidth(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "window pos x/y: " +ofToString(ofGetWindowPositionX(), 2) + '/'
                              +ofToString(ofGetWindowPositionY(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

}



//--------------------------------------------------------------
void ofApp::draw()
{
  ofBackground(0,0,0,0);

  cam.begin();

  ofSetColor(0,128,255,100);
  ofPushMatrix();
  ofRotateY(13.3);
  for (int i = 0; i<planet.getVertices().size();i++)
  {
    ofDrawSphere(planet.getVertex(i).x,planet.getVertex(i).y,planet.getVertex(i).z, .1);
  }
  ofPopMatrix();

  ofSetColor(0,128,255,150);
  ofPushMatrix();
  ofRotateY(27.3);
  for (int i = 0; i<planet2.getVertices().size();i++)
  {
    ofDrawSphere(planet2.getVertex(i).x,planet2.getVertex(i).y,planet2.getVertex(i).z, .125);
  }
  ofPopMatrix();

  ofSetColor(0,128,255,200);
  ofPushMatrix();
  ofRotateY(56.3);
  for (int i = 0; i<planet3.getVertices().size();i++)
  {
    ofDrawSphere(planet3.getVertex(i).x,planet3.getVertex(i).y,planet3.getVertex(i).z, .175);
  }
  ofPopMatrix();

  ofSetColor(0,128,255,255);
  ofPushMatrix();
  ofRotateY(127.3);
  for (int i = 0; i<planet4.getVertices().size();i++)
  {
    ofDrawSphere(planet4.getVertex(i).x,planet4.getVertex(i).y,planet4.getVertex(i).z, .2);
  }
  ofPopMatrix();

  ofSetColor(255,0,128,100);
  ofPushMatrix();
  ofRotateY(13.3);
  for (int i = 0; i<planet.getVertices().size();i++)
  {
    ofDrawSphere(planet.getVertex(i).x,planet.getVertex(i).y,planet.getVertex(i).z, .1);
  }
  ofPopMatrix();

  ofRotateZ(37.3);
  ofTranslate(70,0,0);

  ofSetColor(255,0,128,150);
  ofPushMatrix();
  ofRotateY(27.3);
  for (int i = 0; i<planet2.getVertices().size();i++)
  {
    ofDrawSphere(planet2.getVertex(i).x,planet2.getVertex(i).y,planet2.getVertex(i).z, .125);
  }
  ofPopMatrix();

  ofSetColor(255,0,128,200);
  ofPushMatrix();
  ofRotateY(56.3);
  for (int i = 0; i<planet3.getVertices().size();i++)
  {
    ofDrawSphere(planet3.getVertex(i).x,planet3.getVertex(i).y,planet3.getVertex(i).z, .175);
  }
  ofPopMatrix();

  ofSetColor(255,0,128,255);
  ofPushMatrix();
  ofRotateY(56.3);
  for (int i = 0; i<planet4.getVertices().size();i++)
  {
    ofDrawSphere(planet4.getVertex(i).x,planet4.getVertex(i).y,planet4.getVertex(i).z, .2);
  }
  ofPopMatrix();

  cone.drawWireframe();
  ofSetColor(255,0,0,255);
  cone2.drawWireframe();
  ofSetColor(0,0,255,255);
  cone3.drawWireframe();
  ofSetColor(0,255,255,255);
  cone4.drawWireframe();
  
  //void ofDrawGrid(float stepSize=1.25f, size_t numberOfSteps, bool labels=false, bool x=true, bool y=true, bool z=true)
  //ofSetColor(0,125,255,128);
	//ofDrawGrid(20.0f, 20, true, true, true, true);
  ofDrawGridPlane(20.0f, 20, true);

	//myoperationalarea.draw_model();

	//draw nodes
	for(int i=0;i<num_centroids;i++)
  {
		centroids[i].FACES=gui.FACES;
	  centroids[i].POINTS=gui.POINTS;
	  centroids[i].EDGES=gui.EDGES;
		centroids[i].sphere_radius = gui.sphere_radius_global;
    centroids[i].draw_node();
  }

	//Draw links between nodes
	if(gui.drawlinks)
	{
		for(int i=0;i<num_centroids;i++)
		{
			for(int j=i;j<num_centroids;j++)
			{
				ofSetColor((int)(link_color[0]*255),
									 (int)(link_color[1]*255),
									 (int)(link_color[2]*255),
									 (int)(link_color[3]*255));
				ofDrawLine(centroids[i].coord,centroids[j].coord);
			}

		}
	}

  cam.end();

  if (gui.DIAGNOSTICS)DisplayDiagnostics();
  DisplayIntersections();
  gui.GUIDisplay();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
//This function gets called when the any mouse button is pushed down
void ofApp::mousePressed(int x, int y, int button){

  //we are on IMGUI, disable other mouse inputs, enable again in MouseReleased
  if (ImGui::GetIO().WantCaptureMouse)
  {
    cam.disableMouseInput();        //disable the camera movement
  }

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

    cam.enableMouseInput();        //enable the camera movement

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
