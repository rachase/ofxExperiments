#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"
#include "ofxAssimpModelLoader.h"



class MdsGui : public ofxImGui::Gui
{

  public:

  // Gui Components
  void GUIDisplay(void); 		//For organization purposes
  void SetupGUI(void);         //initial setup parameters

  void ShowFileMenu();

  bool drawlinks = TRUE;
  bool resetnodes = FALSE;
  bool POINTS=TRUE;
  bool EDGES=TRUE;
  bool FACES=TRUE;

  bool DIAGNOSTICS=TRUE;
  int node_count = 10;
  float sphere_radius_global;
  float boundary;

  //camera parameters
  bool MOUSEINPUT  = TRUE;
  bool MOUSEMIDDLE = TRUE;

  bool cam_sliders = FALSE;  //overrides easy cam to use sliders
  float cam_position_x;
  float cam_position_y;
  float cam_position_z;
  float cam_rotation_x;
  float cam_rotation_y;
  float cam_rotation_z;
  float cam_target_x;
  float cam_target_y;
  float cam_target_z;

  int model_file = 0; //Id of current Model

};
