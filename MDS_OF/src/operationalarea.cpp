#include "operationalarea.h"

void operationalarea::draw_model()
{
  ofPushMatrix();
  model.setScaleNormalization(TRUE);
  model.setScale(modelscale,modelscale,modelscale);
  model.setPosition(0,0,0);
  // model.setRotation(0,rot.x,1,0,0);
  // model.setRotation(1,rot.y,0,1,0);
  // model.setRotation(2,rot.z,0,0,1);


  if(FACES)
  {
       ofSetColor(facecolor);
       model.drawFaces();
  }
  if(EDGES)
  {
       ofSetColor(edgecolor);
       model.drawWireframe();
  }
  if(POINTS)
  {
       ofSetColor(vertcolor);
       model.drawVertices();
  }
  ofPopMatrix();
}

void operationalarea::draw_gui()
{
  ImGui::Text("Model");
  ImGui::Checkbox("FACES", &FACES);
  ImGui::SameLine();
  ImGui::Checkbox("EDGES", &EDGES);
  ImGui::SameLine();
  ImGui::Checkbox("POINTS", &POINTS);
}

void operationalarea::load_model()
{
  bool check;
  check = model.loadModel("/home/richard/Desktop/ccast_deci.stl");
  if (check)
  {
    printf("Model Loaded\n");
  }
}

//not fully implented
void operationalarea::setup()
{
  FACES=TRUE;
  EDGES=TRUE;
  POINTS=TRUE;
}
