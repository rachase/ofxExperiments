#include "ant_node.h"

//http://www.ambrsoft.com/TrigoCalc/Sphere/TwoSpheres/Intersection.htm
void ofAntNode::getintersection(ofAntNode * othersphere)
{
  double d,r1,r2,d_2,r1_2,r2_2;
  double cap1,cap2;
  ofVec3f N = othersphere->coord;
  d = coord.distance(othersphere->coord);  //distance
  d_2 = d*d;
  r1 = sphere_radius;
  r1_2 = r1*r1;
  r2 = othersphere->sphere_radius;
  r2_2 = r2*r2;

  double alpha = acos((r1_2+d_2-r2_2)/(2*r1*d));
  double theta = acos((r2_2+d_2-r1_2)/(2*r2*d));
  double h  = r1*sin(alpha);
  cap1 = r1 * (1-cos(alpha));
  cap2 = r2 * (1-cos(theta));

  //intersection plane
  double A = 2*(coord.x-othersphere->coord.x);
  double B = 2*(coord.y-othersphere->coord.y);
  double C = 2*(coord.z-othersphere->coord.z);
  double D = coord.x*coord.x-N.x*N.x+
             coord.y*coord.y-N.y*N.y+
             coord.z*coord.z-N.z*N.z+
             -sphere_radius*sphere_radius
             +othersphere->sphere_radius*othersphere->sphere_radius;

  double t = (coord.x*A+coord.y*B+coord.z*C + D) /
      (A*(coord.x-N.x)+B*(coord.y-N.y)+C*(coord.z-N.z));

  bool intersection,seperation,outertanget,inside,innertanget;
  intersection = d < ((r1 + r2) || abs(r1-r2)) ? TRUE : FALSE;
  seperation = d > (r1 - r2) ? TRUE : FALSE;
  outertanget = d == (r1 + r2) ? TRUE : FALSE;
  inside = d < abs(r1 - r2) ? TRUE : FALSE;
  innertanget = d == abs(r1 - r2) ? TRUE : FALSE;

}

void ofAntNode::populate(float range)
{
  float scratch = range/2;
  coord.set(ofRandom(-scratch,scratch),ofRandom(-scratch,scratch),ofRandom(-scratch,scratch));
  rot.set(ofRandom(0,360),ofRandom(0,360),ofRandom(0,360));

  //for drawing sphere
  sphere.setRadius(10);
  sphere.setPosition(coord);
  sphere.rotate(rot.x, 1.0, 0.0, 0.0);
  sphere.rotate(rot.y, 0.0, 1.0, 0.0);
  sphere.rotate(rot.z, 0.0, 0.0, 1.0);

}

void ofAntNode::draw(ofxAssimpModelLoader * thismodel, float thisscale)
{
   thismodel->setScaleNormalization(TRUE);
   thismodel->setScale(thisscale,thisscale,thisscale);
   thismodel->setPosition(coord.x,coord.y,coord.z);
   thismodel->setRotation(0,rot.x,1,0,0);
   thismodel->setRotation(1,rot.y,0,1,0);
   thismodel->setRotation(2,rot.z,0,0,1);

   if(FACES)
   {
       ofSetColor(facecolor);
       thismodel->drawFaces();
   }
   if(EDGES)
   {
       ofSetColor(edgecolor);
       thismodel->drawWireframe();
   }

   if(POINTS)
   {
       ofSetColor(vertcolor);
       thismodel->drawVertices();
   }
}

void ofAntNode::draw_sphere(float thisscale)
{
  ofPushMatrix();
    sphere.setRadius(10);
    if(FACES)
    {
        ofSetColor(facecolor);
        sphere.draw();
    }
    if(EDGES)
    {
        ofSetColor(edgecolor);
        sphere.drawWireframe();
    }

    if(POINTS)
    {
        ofSetColor(vertcolor);
        sphere.drawVertices();
    }
  ofPopMatrix();
}

void ofAntNode::draw_node()
{
  ofMesh triangles = sphere.getMesh();

  ofPushMatrix();
    //triangles.draw();
    sphere.setRadius(sphere_radius);
    if(FACES)
    {
        ofSetColor(facecolor);
        sphere.draw();
    }
    if(EDGES)
    {
        ofSetColor(edgecolor);
        sphere.drawWireframe();
    }

    if(POINTS)
    {
        ofSetColor(vertcolor);
        sphere.drawVertices();
    }
  ofPopMatrix();
}

void ofAntNode::levy(void)
{
}
