#include "MdsGui.h"

//list of models in folder. Can set this to be dynamic later
const char * MODEL_NAMES[] {
	"RDRR_ASCI",
	"Elephant",
	"Lab",
	"Bear",
	"Moose",
	"RDRR_BIN",
	"Wolf",
	"Sphere"
};

const char * MODEL_STYLE[] {
	"POINTS",
	"LINES",
	"MESH",
	"SOLID",
};

bool show_test_window;


ofxAssimpModelLoader model2;

void MdsGui::SetupGUI()
{
  //set window properties
  static bool no_titlebar = false;
  static bool no_border = true;
  static bool no_resize = true;
  static bool no_move = true;
  static bool no_scrollbar = false;
  static bool no_collapse = true;
  static bool no_menu = true;
  static bool no_settings = true;
  static float bg_alpha = -0.01f; // <0: default
  bool show = true;

  int myWidth = 10;
  int myHeight = 20;
  // Demonstrate the various window flags.
  // Typically you would just use the default.
  ImGuiWindowFlags window_flags = 0;
  if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
  //if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
  if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
  if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
  if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
  if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;
  if (!no_menu)     window_flags |= ImGuiWindowFlags_MenuBar;
  if (no_settings) window_flags |= ImGuiWindowFlags_NoSavedSettings;

  //ImGui::Begin("My Title", &show, ImVec2(myWidth, myHeight), bg_alpha, window_flags);

}

void MdsGui::ShowFileMenu()
{
	    ImGui::MenuItem("(dummy menu)", NULL, false, false);
	    if (ImGui::MenuItem("New")) {}
	    if (ImGui::MenuItem("Open", "Ctrl+O")) {}
	    if (ImGui::BeginMenu("Open Recent"))
	    {
	        ImGui::MenuItem("fish_hat.c");
	        ImGui::MenuItem("fish_hat.inl");
	        ImGui::MenuItem("fish_hat.h");
	        ImGui::EndMenu();
	    }
	    if (ImGui::MenuItem("Save", "Ctrl+S")) {}
	    if (ImGui::MenuItem("Save As..")) {}
	    ImGui::Separator();
	    if (ImGui::BeginMenu("Options"))
	    {
	        static bool enabled = true;
	        ImGui::MenuItem("Enabled", "", &enabled);
	        ImGui::BeginChild("child", ImVec2(0, 60), true);
	        for (int i = 0; i < 10; i++)
	            ImGui::Text("Scrolling Text %d", i);
	        ImGui::EndChild();
	        static float f = 0.5f;
	        static int n = 0;
	        static bool b = true;
	        ImGui::SliderFloat("Value", &f, 0.0f, 1.0f);
	        ImGui::InputFloat("Input", &f, 0.1f);
	        ImGui::Combo("Combo", &n, "Yes\0No\0Maybe\0\0");
	        ImGui::Checkbox("Check", &b);
	        ImGui::EndMenu();
	    }
	    if (ImGui::BeginMenu("Disabled", false)) // Disabled
	    {
	        IM_ASSERT(0);
	    }
	    if (ImGui::MenuItem("Checked", NULL, true)) {}
	    if (ImGui::MenuItem("Quit", "Alt+F4")) {}

}

void MdsGui::GUIDisplay()
{
  static int current_model = -1;

	// Display GUI
  begin();

	// Menu
	if (ImGui::BeginMenuBar())
	{
			if (ImGui::BeginMenu("Menu"))
			{
					ShowFileMenu();
					ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Examples"))
			{
					ImGui::MenuItem("Main menu bar", NULL, &show_test_window);
					ImGui::MenuItem("Console", NULL, &show_test_window);
					ImGui::MenuItem("Log", NULL, &show_test_window);
					ImGui::MenuItem("Simple layout", NULL, &show_test_window);
					ImGui::MenuItem("Property editor", NULL, &show_test_window);
					ImGui::MenuItem("Long text display", NULL, &show_test_window);
					ImGui::MenuItem("Auto-resizing window", NULL, &show_test_window);
					ImGui::MenuItem("Constrained-resizing window", NULL, &show_test_window);
					ImGui::MenuItem("Simple overlay", NULL, &show_test_window);
					ImGui::MenuItem("Manipulating window titles", NULL, &show_test_window);
					ImGui::MenuItem("Custom rendering", NULL, &show_test_window);
					ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Help"))
			{
					ImGui::MenuItem("Metrics", NULL, &show_test_window);
					ImGui::MenuItem("Style Editor", NULL, &show_test_window);
					ImGui::MenuItem("About Dear ImGui", NULL, &show_test_window);
					ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
	}


	ImGui::Checkbox("Show Test Window", &show_test_window);
	if(show_test_window)
	{
			ImGui::ShowDemoWindow(&show_test_window);
	}


  ImGui::Spacing();
	if(ImGui::CollapsingHeader("Links"))
	{
		//ImGui::ColorEdit4("Color", (float *)&link_color);
		ImGui::Checkbox("Node Links", &drawlinks);
	}


	if(ImGui::CollapsingHeader("Nodes"))
	{
		ImGui::SliderInt("Node Count", &node_count, 0, 256);
		ImGui::SliderFloat("Boundary", &boundary, 0, 100);
		ImGui::SliderFloat("Model Scale", &sphere_radius_global, 0.0f, 50.0f);
		if(ImGui::Button("Reset Nodes"))
		{
				resetnodes = true;
		}
	}

	if(ImGui::CollapsingHeader("Node Models"))
	{
		//ImGui::ColorEdit4("Color", (float *)&model_color);
		ImGui::Checkbox("FACES", &FACES);
		ImGui::SameLine();
		ImGui::Checkbox("EDGES", &EDGES);
		ImGui::SameLine();
		ImGui::Checkbox("POINTS", &POINTS);

		ImGui::Combo("Model", &model_file, MODEL_NAMES, 8);
		//ImGui::SliderFloat("Model Scale", &model_scale, 0.0f, 1.0f);
	}

	ImGui::Combo("Ground Model", &model_file, MODEL_NAMES, 8);


  if(ImGui::CollapsingHeader("Easy Cam"))
  {
    ImGui::Checkbox("Easy Cam Sliders", &cam_sliders);
    if(cam_sliders==TRUE)
    {
      ImGui::SliderFloat("POSX", &cam_position_x, -800.0f, 1600.0f);
      ImGui::SliderFloat("POSY", &cam_position_y, -800.0f, 1600.0f);
      ImGui::SliderFloat("POSZ", &cam_position_z, -800.0f, 1600.0f);
      ImGui::Spacing();
      ImGui::SliderFloat("TARX", &cam_target_x, -100,100);
      ImGui::SliderFloat("TARY", &cam_target_y, -100,100);
      ImGui::SliderFloat("TARZ", &cam_target_z, -100,100);
      ImGui::Spacing();
      ImGui::SliderFloat("ROTX", &cam_rotation_x, 0.0,360.0);
      ImGui::SliderFloat("ROTY", &cam_rotation_y, 0.0,360.0);
      ImGui::SliderFloat("ROTZ", &cam_rotation_z, 0.0,360.0);
    }

	}

  //see if the current model has been loaded
  //if not, load model and check if successful
  //else skip
  bool check = 0;
  if (current_model != model_file)
  {
    switch(model_file)
    {
      case 0: check = model2.loadModel("/media/rachase/Data/FunModels/RDRR_b.stl");
      break;
      case 1: check = model2.loadModel("/media/rachase/Data/FunModels/Elephant.stl");
      break;
      case 2: check = model2.loadModel("/media/rachase/Data/FunModels/Pik.stl");
      break;
      case 3: check = model2.loadModel("/media/rachase/Data/FunModels/Bear.stl");
      break;
      case 4: check = model2.loadModel("/media/rachase/Data/FunModels/Moose.stl");
      break;
      case 5: check = model2.loadModel("/media/rachase/Data/FunModels/RDRR_a.stl");
      break;
      case 6: check = model2.loadModel("/media/rachase/Data/FunModels/Wolf.stl");
      break;
      default: check = model2.loadModel("/media/rachase/Data/FunModelsBunny-LowPoly.stl");
      break;
    }
    if (check)
    {
      current_model = model_file;
      printf("Model Loaded\n");
    }
  }



  end();
}
