#pragma once
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"
#include "ofxAssimpModelLoader.h"
class operationalarea
{
  public:
  //lats and longs as indicated.
  double max_lattitude;
  double max_longitude;
  double max_altitude;
  double min_lattitude;
  double min_longitude;
  double min_altitude;
  double center_lattitude;
  double center_longitude;
  double center_altitude;

  //for registration. This will be 0,0,0 in OF
  double model_origin_lattitude;
  double model_origin_longitude;
  double model_origin_altitude;

  //default model colors
  ofFloatColor facecolor={1.0f,1.0f,1.0f,1.0f};
  ofFloatColor edgecolor={0.7f,0.7f,0.7f,1.0f};
  ofFloatColor vertcolor={0.0f,0.0f,0.0f,1.0f};

  ofVec3f coord;
  ofVec3f rot;

  //default shape will be a grid
  ofSpherePrimitive sphere;

  ofxAssimpModelLoader model;
  float modelscale=1;
  void draw_model();      //draws the model to the screen
  void draw_gui();        //draws the imgui
  void load_model();      //loads the model and updates parameters.
  void setup();           //init default settings

  bool FLAT_RENDER=TRUE;  //use lighting or flat
  bool POINTS=FALSE;
  bool EDGES=TRUE;
  bool FACES=FALSE;


};
