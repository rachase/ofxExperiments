#pragma once
#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
class ofAntNode
{

  public:
  ofVec3f coord;
  ofVec3f rot;

  ofFloatColor facecolor={0.0f,0.5f,1.0f,1.0f};
  ofFloatColor edgecolor={0.0f,1.0f,0.5f,1.0f};
  ofFloatColor vertcolor={1.0f,1.0f,0.0f,1.0f};

  //sphere for drawing purposes.  Change to mesh later
  ofSpherePrimitive sphere;
  float sphere_radius = 10.0;

  void draw(ofxAssimpModelLoader * thismodel, float thisscale);
  void populate(float range);
  void levy(void);
  void draw_sphere(float thisscale);
  void draw_node(void);
  void getintersection(ofAntNode * B);
  bool POINTS=FALSE;
  bool EDGES=TRUE;
  bool FACES=FALSE;
};
