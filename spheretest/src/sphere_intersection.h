#pragma once
#include "ofMain.h"
#include "ofxAssimpModelLoader.h"

class ofSphereIntersection
{
  public:
    double d;  //distance between centers
    double d2; //distance squared
    double r1; //radius of base object
    double r2; //radius of target object

    // squared values for quick calc
    double x12; // x coord of base object ^ 2
    double x22; // x coord of target object ^ 2
    double y12;
    double y22;
    double z12;
    double z22;
    double r12;
    double r22;

    //equation for intersection plane.
    double A;
    double B;
    double C;
    double D;

    double alpha;
    double theta;
    double h;   // radius of intersection circle
    double h1;  //cap 1 height
    double h2;  //cap 2 height

    //collision tests;
    bool intersection;
    bool seperate;
    bool outertanget;
    bool inside;
    bool innertanget;

    //center point of circle
    double t;

    ofVec3f circ_center;  //center point of circle
    ofVec4f rotation;     //rotation to base object

    ofFloatColor facecolor={0.0f,0.5f,1.0f,.10f};
    ofFloatColor edgecolor={0.0f,1.0f,0.5f,.5f};
    ofFloatColor vertcolor={1.0f,1.0f,0.0f,.6f};

    void draw(void);
    void computeintersection(ofSpherePrimitive *k , ofSpherePrimitive *n);

    bool POINTS=FALSE;
    bool EDGES=TRUE;
    bool FACES=FALSE;

};
