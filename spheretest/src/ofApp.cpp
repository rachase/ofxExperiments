#include "ofApp.h"

ofxAssimpModelLoader model;

ofFloatColor link_color= { 1.0f,0.0f,0.25f,0.5f };
ofFloatColor model_color= { 0.0f,1.0f,0.50f,1.0f };

void ofApp::setLightOri(ofLight &light, ofVec3f rot)
{
    ofVec3f xax(1, 0, 0);
    ofVec3f yax(0, 1, 0);
    ofVec3f zax(0, 0, 1);
    ofQuaternion q;
    q.makeRotate(rot.x, xax, rot.y, yax, rot.z, zax);
    light.setOrientation(q);
}

//--------------------------------------------------------------
void ofApp::setup()
{

//  ofEnableDepthTest();

  //ofSetVerticalSync(true);
  ofSetFrameRate(30);
  ofEnableDepthTest();
  ofEnableAlphaBlending();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  //setup the camera
	cam.setup();

  //initialize centroid positions
  //NOTE: Make this a function of data class later
  for(int i=0;i<MAX_NODES;i++)
  {
    centroids[i].setup(box_size);
  }

  //setup the GUI
  gui.setup();
}

//--------------------------------------------------------------
void ofApp::update(){

  static int current_model = -1;
  //reset all the node positions
  if(gui.resetnodes)
  {
    gui.resetnodes = false;
    for(int i=0;i<MAX_NODES;i++)
    {
      centroids[i].setup(box_size);
    }
  }

  for(int i=0;i<MAX_NODES;i++)
  {
    centroids[i].radius = gui.sphere_radius_global;
  }


  //change the number of centroids from gui slider
	num_centroids = gui.node_count;
	//cam.setPosition(gui.cam_position_x,gui.cam_position_y,gui.cam_position_z);
  //cam.setOrientation(ofVec3f(gui.cam_rotation_x,gui.cam_rotation_y,gui.cam_rotation_z));

  intersectioncenters.clear();
  for(int i=1;i<num_centroids;i++)
  {
    intersectioncenters.push_back(centroids[i].addintersectionplane(&centroids[i-1]));
  }
//  cam.setTarget(ofVec3f(gui.cam_target_x,gui.cam_target_y,gui.cam_target_z));

}


void ofApp::DisplayDiagnostics()
{
  string fpsStr;
  int base_x=100,base_y=100;
  int space=12;
  int i=0;

  base_x = ofGetWindowWidth()-260;
  base_y = ofGetWindowHeight()-75;
  ofSetColor(255,255,255,255);
  i++;

  //frame rate Statistics
  fpsStr = "frame rate/target: "
          +ofToString(ofGetFrameRate(), 2) + '/'
          +ofToString(ofGetTargetFrameRate(), 2);

  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "frame num: "+ofToString(ofGetFrameNum(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "last frame time: "+ofToString(ofGetLastFrameTime(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  //Window Statistics
  fpsStr = "window h/w: " +ofToString(ofGetWindowHeight(), 2) + '/'
                          +ofToString(ofGetWindowWidth(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

  fpsStr = "window pos x/y: " +ofToString(ofGetWindowPositionX(), 2) + '/'
                              +ofToString(ofGetWindowPositionY(), 2);
  ofDrawBitmapString(fpsStr, base_x,base_y+space*i);
  i++;

}

//--------------------------------------------------------------
void ofApp::draw()
{
  ofBackground(0,0,0,0);
  ofVec4f scratch;
  ofVec3f temp;
  float temp2;
  cam.begin();
  //void ofDrawGrid(float stepSize=1.25f, size_t numberOfSteps, bool labels=false, bool x=true, bool y=true, bool z=true)
  ofSetColor(0,125,255,128);
  ofDrawGridPlane(20.0f, 20, false);


  for(int i=0;i<num_centroids;i++)
  {
    for(int j=1;j<num_centroids;j++)
    {
      ofSetColor(255,255,255,128);
      scratch = centroids[i].addintersectionplane(&centroids[j]);
      if((scratch.x!=0) & (scratch.y!=0) & (scratch.z!=0))
      {
        ofPushMatrix();
          ofTranslate(scratch.x,scratch.y,scratch.z);
          //ofRotateXRad(acos(scratch.x-centroids[i].center.x));
          //ofRotateYRad(acos(scratch.y-centroids[i].center.y));
          //ofRotateZRad(acos(scratch.y-centroids[i].center.z));
          temp2=scratch.w;
          ofDrawCircle(0,0,0,temp2);
          //ofTranslate(-scratch.x,-scratch.y,-scratch.z);
        ofPopMatrix();
      }
    }

  }
/*
  for(int i=0;i<intersectioncenters.size();i++)
  {

    ofSetColor(255,255,255,255);
    ofDrawBox(intersectioncenters.at(i),.5);
  }
*/
	//draw nodes
	for(int i=0;i<num_centroids;i++)
  {
		centroids[i].FACES=gui.FACES;
	  centroids[i].POINTS=gui.POINTS;
	  centroids[i].EDGES=gui.EDGES;
		centroids[i].radius = gui.sphere_radius_global;
    centroids[i].draw();
  }


	//Draw links between nodes
	if(gui.drawlinks)
	{
		for(int i=0;i<num_centroids;i++)
		{
			for(int j=i;j<num_centroids;j++)
			{
				ofSetColor((int)(link_color[0]*255),
									 (int)(link_color[1]*255),
									 (int)(link_color[2]*255),
									 (int)(link_color[3]*255));
				ofDrawLine(centroids[i].center,centroids[j].center);
			}

		}
	}

  cam.end();

  if (gui.DIAGNOSTICS)DisplayDiagnostics();
  gui.GUIDisplay();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
