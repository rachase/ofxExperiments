#include "MdsGui.h"


void MdsGui::SetupGUI()
{
  //set window properties
  static bool no_titlebar = false;
  static bool no_border = true;
  static bool no_resize = true;
  static bool no_move = true;
  static bool no_scrollbar = false;
  static bool no_collapse = true;
  static bool no_menu = true;
  static bool no_settings = true;
  static float bg_alpha = -0.01f; // <0: default
  bool show = true;

  int myWidth = 10;
  int myHeight = 20;
  // Demonstrate the various window flags.
  // Typically you would just use the default.
  ImGuiWindowFlags window_flags = 0;
  if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
  //if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
  if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
  if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
  if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
  if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;
  if (!no_menu)     window_flags |= ImGuiWindowFlags_MenuBar;
  if (no_settings)  window_flags |= ImGuiWindowFlags_NoSavedSettings;

  ImGui::Begin("My Title", &show, window_flags);

}
void MdsGui::GUIDisplay()
{
  // Display GUI
  begin();
  ImGui::Spacing();
	//if(ImGui::CollapsingHeader("Links"))
	//{
	//	ImGui::ColorEdit4("Color", (float *)&link_color);
		ImGui::Checkbox("Node Links", &drawlinks);
	//}


	//if(ImGui::CollapsingHeader("Nodes"))
	//{
		ImGui::SliderInt("Node Count", &node_count, 0, 256);
		ImGui::SliderFloat("Model Scale", &sphere_radius_global, 0.0f, 50.0f);
		if(ImGui::Button("Reset Nodes"))
		{
				resetnodes = true;
		}
	//}

	//if(ImGui::CollapsingHeader("Node Models"))
	//{
		//ImGui::ColorEdit4("Color", (float *)&model_color);
		ImGui::Checkbox("FACES", &FACES);
		ImGui::SameLine();
		ImGui::Checkbox("EDGES", &EDGES);
		ImGui::SameLine();
		ImGui::Checkbox("POINTS", &POINTS);

		//ImGui::Combo("Model", &model_file, MODEL_NAMES, 8);
		//ImGui::SliderFloat("Model Scale", &model_scale, 0.0f, 1.0f);
	//}

	//if(ImGui::CollapsingHeader("Ground Model"))
	//{
	//	ImGui::Text("TBD");
	//}
/*
  if(ImGui::CollapsingHeader("Easy Cam"))
  {
		ImGui::SliderFloat("POSX", &cam_position_x, -800.0f, 1600.0f);
    ImGui::SliderFloat("POSY", &cam_position_y, -800.0f, 1600.0f);
    ImGui::SliderFloat("POSZ", &cam_position_z, -800.0f, 1600.0f);
    ImGui::Spacing();
    ImGui::SliderFloat("TARX", &cam_target_x, -100,100);
    ImGui::SliderFloat("TARY", &cam_target_y, -100,100);
    ImGui::SliderFloat("TARZ", &cam_target_z, -100,100);
    ImGui::Spacing();
    ImGui::SliderFloat("ROTX", &cam_rotation_x, 0.0,360.0);
    ImGui::SliderFloat("ROTY", &cam_rotation_y, 0.0,360.0);
    ImGui::SliderFloat("ROTZ", &cam_rotation_z, 0.0,360.0);
	}
*/
  end();
}
