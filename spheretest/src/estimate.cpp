#include "estimate.h"

ofVec4f ofEstimate::addintersectionplane(ofEstimate *n)
{
  //http://www.ambrsoft.com/TrigoCalc/Sphere/TwoSpheres/Intersection.htm

  double d  = center.distance(n->center);
  double d2 = d*d;
  double r1 = radius;
  double r2 = n->radius;

  double x12 = center.x*center.x;
  double x22 = n->center.x*n->center.x;
  double y12 = center.y*center.y;
  double y22 = n->center.y*n->center.y;
  double z12 = center.z*center.z;
  double z22 = n->center.z*n->center.z;
  double r12 = radius*radius;
  double r22 = n->radius*n->radius;

  //equation for intersection plane.
  double A = 2*(n->center.x-center.x);
  double B = 2*(n->center.y-center.y);
  double C = 2*(n->center.z-center.z);
  double D = x12 - x22 + y12 - y22 + z12 - z22 - r12 + r22;

  double alpha = acos((r12+d2-r22)/(2*r1*d));
  double theta = acos((r22+d2-r12)/(2*r2*d));
  double h = r1 * sin(alpha);
  double h1 = r1 * (1- cos(alpha));   // cap 1
  double h2 = r2 * (1- cos(theta));   // cap 2

  //collision tests;
  bool intersection = ((d < (r1 + r2)) & (d > abs(r1-r2))) ? TRUE : FALSE;
  bool seperate = (d > (r1+r2)) ? TRUE : FALSE;
  bool outertanget = (d==(r1+r2)) ? TRUE : FALSE;
  bool inside = (d<abs(r1-r2))? TRUE : FALSE;
  bool innertanget = (d==(r1+r2)) ? TRUE : FALSE;

  //center point of circle
  double t = (center.x*A+center.y*B+center.z*C + D)/
             (A*(center.x - n->center.x)+B*(center.y - n->center.y)+C*(center.z - n->center.z));

  ofVec4f circ_center;
  circ_center.x = center.x + t*(n->center.x - center.x);
  circ_center.y = center.y + t*(n->center.y - center.y);
  circ_center.z = center.z + t*(n->center.z - center.z);
  circ_center.w = h;

  if (intersection)  return circ_center;
  else return ofVec3f(0,0,0);

}

void ofEstimate::clearintersectionplane(ofEstimate *n)
{

}




void ofEstimate::setup(float range)
{
  float scratch = range/2;
  center.set(ofRandom(-scratch,scratch),ofRandom(-scratch,scratch),ofRandom(-scratch,scratch));
  rot.set(ofRandom(0,360),ofRandom(0,360),ofRandom(0,360));

  //for drawing sphere
  sphere.setRadius(radius);
  sphere.setPosition(center);
  sphere.rotate(rot.x, 1.0, 0.0, 0.0);
  sphere.rotate(rot.y, 0.0, 1.0, 0.0);
  sphere.rotate(rot.z, 0.0, 0.0, 1.0);

}

void ofEstimate::draw()
{
  int i;
  ofPushMatrix();
    sphere.setRadius(radius);
    if(FACES)
    {
        ofSetColor(facecolor);
        sphere.draw();
    }
    if(EDGES)
    {
        ofSetColor(edgecolor);
        sphere.drawWireframe();
    }

    if(POINTS)
    {
        ofSetColor(vertcolor);
        sphere.drawVertices();
    }
    //ofSetColor(ofFloatColor(1.0f,1.0f,0.0f,1.0f));
    //ofDrawCircle(center.x,center.y,center.z,5);
  ofPopMatrix();


}
