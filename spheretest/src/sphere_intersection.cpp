#include "sphere_intersection.h"

void ofSphereIntersection::computeintersection(ofSpherePrimitive *k , ofSpherePrimitive *n)
{
  //http://www.ambrsoft.com/TrigoCalc/Sphere/TwoSpheres/Intersection.htm
  ofVec3f kpos = k->getGlobalPosition();
  ofVec3f npos = k->getGlobalPosition();
  d  = kpos.distance(npos);
  d2 = d*d;
  r1 = k->getRadius();
  r2 = n->getRadius();

  x12 = k->getX()*k->getX();
  x22 = n->getX()*n->getX();
  y12 = k->getY()*k->getY();
  y22 = n->getY()*n->getY();
  z12 = k->getZ()*k->getZ();
  z22 = n->getZ()*n->getZ();
  r12 = k->getRadius()*k->getRadius();
  r22 = n->getRadius()*n->getRadius();

  //equation for intersection plane.
  A = 2*(n->getX()-k->getX());
  B = 2*(n->getY()-k->getY());
  C = 2*(n->getZ()-k->getZ());
  D = x12 - x22 + y12 - y22 + z12 - z22 - r12 + r22;

  alpha = acos((r12+d2-r22)/(2*r1*d));
  theta = acos((r22+d2-r12)/(2*r2*d));
  h = r1 * sin(alpha);
  h1 = r1 * (1- cos(alpha));   // cap 1
  h2 = r2 * (1- cos(theta));   // cap 2

  //collision tests;
  intersection = ((d < (r1 + r2)) & (d > abs(r1-r2))) ? TRUE : FALSE;
  seperate = (d > (r1+r2)) ? TRUE : FALSE;
  outertanget = (d==(r1+r2)) ? TRUE : FALSE;
  inside = (d<abs(r1-r2))? TRUE : FALSE;
  innertanget = (d==(r1+r2)) ? TRUE : FALSE;

  //center point of circle
  t = (k->getX()*A+k->getY()*B+k->getZ()*C + D)/
  (A*(k->getX() - n->getX())+B*(k->getY() - n->getY())+C*(k->getZ() - n->getZ()));


  circ_center.x = k->getX() + t*(n->getX() - k->getX());
  circ_center.y = k->getY() + t*(n->getY() - k->getY());
  circ_center.z = k->getZ() + t*(n->getZ() - k->getZ());

}


void ofSphereIntersection::draw()
{
  ofPushMatrix();
    ofDrawCircle(circ_center, h);
  ofPopMatrix();
}
