#pragma once
#include "ofMain.h"
#include "ofxAssimpModelLoader.h"


class ofEstimate
{
  public:
  ofVec3f center;
  ofVec3f rot;

  ofFloatColor facecolor={0.0f,0.5f,1.0f,.10f};
  ofFloatColor edgecolor={0.0f,1.0f,0.5f,.5f};
  ofFloatColor vertcolor={1.0f,1.0f,0.0f,.6f};

  //sphere for drawing purposes.  Change to mesh later
  ofSpherePrimitive sphere;
  double radius = 10.0;

  void setup(float range);
  void draw(void);
  void clearintersectionplane(ofEstimate *n);
  ofVec4f addintersectionplane(ofEstimate *n);

  bool POINTS=FALSE;
  bool EDGES=TRUE;
  bool FACES=FALSE;

};
