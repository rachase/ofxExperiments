#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    gui.setup();
    gui.add(uiPosition.set("position",ofVec3f(0,0,0),ofVec3f(-300,-300,-300),
            ofVec3f(300,300,300)));    
}

//--------------------------------------------------------------
void ofApp::update(){

   light.setPosition(uiPosition->x,uiPosition->y,uiPosition->z);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofEnableDepthTest();
    //turn light on and off
    light.enable();
    //stop and start camera
    camera.begin();
        ofDrawSphere(uiPosition->x,uiPosition->y,uiPosition->z,32);
        ofDrawBox(0,0,0,128);
    camera.end();
    light.disable();
    ofDisableDepthTest();

    gui.draw();

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage m)
{
}
