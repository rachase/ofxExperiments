#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();

    void gotMessage(ofMessage msg);
    ofxPanel gui;
    ofParameter <ofVec3f> uiPosition;
    ofEasyCam camera;
    ofLight light;
};