#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableSmoothing();
    ofSetLineWidth(1);

    mesh.setMode(OF_PRIMITIVE_LINES);
    
    ofSetFrameRate(60);
    
    gui.setup();
    gui.add(camPosition.set(
        "camposition",
        ofVec3f(0,0,0),
        ofVec3f(-5000,-5000,-5000),
        ofVec3f(5000,5000,5000)
        )); 
    gui.add(nodecount.set("node count",10,0,100));
    gui.add(distance.set("distance",1,0,1000));
    gui.add(spacesize.set("spacesize",100,1,1000));
    gui.add(spheresize.set("spheresize",3,1,100));
    gui.add(linewidth.set("linewidth",1,1,100));

}

//---------------------------------------------------------
void ofApp::update(){
    ofSeedRandom(30);
    mesh.clear();
    for(unsigned int i=0;i<nodecount;i++)
    {
        ofVec3f position = ofVec3f(
            ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*.006),0,1,-spacesize,spacesize),
            ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*.006),0,1,-spacesize,spacesize),
            ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*.006),0,1,-spacesize,spacesize));
        mesh.addVertex(position);
        mesh.addColor(ofColor(255));
    }

    for(unsigned int i = 0; i<mesh.getVertices().size();i++)
    {
        glm::vec3 v = mesh.getVertex(i);
        for(unsigned int j = 0; j<mesh.getVertices().size();j++)
        {
            glm::vec3 jdist = mesh.getVertex(j);
            double t = glm::distance(v,jdist);
            //std::cout << v << "|" << jdist << "|" << t << "|" << distance << std::endl;
            if (t<distance)
            {
                mesh.addIndex(i);
                mesh.addIndex(j);
            }
        }
    }

    ofSetLineWidth(linewidth);


}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
   // cam.setGlobalPosition(camPosition.get());
    
    
    //ofTranslate(ofGetHeight()/2,ofGetWidth()/2);
    ofEnableDepthTest();
    cam.begin();
    for(int i = 0; i<mesh.getNumVertices();i++)
    ofDrawSphere(mesh.getVertex(i),spheresize);
    mesh.draw();
        ofDisableDepthTest();
    //ofTranslate(-ofGetHeight()/2,-ofGetWidth()/2);
    cam.end();
    gui.draw();   
}