#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
 
    ofMesh mesh;
    ofEasyCam cam;

    ofxPanel gui;
    ofParameter <ofVec3f> camPosition;
    ofParameter <float> distance;
    ofParameter <int> nodecount;
    ofParameter <float> spacesize;
    ofParameter <float> spheresize;
    ofParameter <float> linewidth;
    
    ofColor colors[20]={
        ofColor::papayaWhip,
        ofColor::plum,
        ofColor::rosyBrown,
        ofColor::seaShell,
        ofColor::sienna,
        ofColor::tomato,
        ofColor::aliceBlue,
        ofColor::aquamarine,
        ofColor::blanchedAlmond,
        ofColor::burlyWood,
        ofColor::navajoWhite,
        ofColor::grey,
        ofColor::hotPink,
        ofColor::honeyDew,
        ofColor::lavenderBlush,
        ofColor::lawnGreen,
        ofColor::lemonChiffon,
        ofColor::mediumOrchid,
        ofColor::oliveDrab,
        ofColor::thistle
        };
    

};