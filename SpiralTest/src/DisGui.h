#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"

class DisGui : public ofxImGui::Gui
{

public:
  // Generic Gui Components
  void GuiDisplay(void); //For organization purposes
  void SetupGui(void);   //initial setup parameters

  bool DIAGNOSTICS = TRUE;
  
  DisGui();

  //variables for this GUI

  float radius;
	float extension;
  float arc_step;
  
  float z_depth;
  float margin_topbot;
  int read_doodle_offset;

  float micro_noise;
  float milli_noise;
  float hundo_noise;
  float int_noise;
  float noise_mult;
  
  void setup_font(void);

};
