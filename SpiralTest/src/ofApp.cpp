#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofEnableSmoothing();
    ofSetLineWidth(1);

    //setup the GUI
    gui.setup();

    save_to_svg_flag = false;
}

//---------------------------------------------------------
void ofApp::update()
{
}

void ofApp::draw_asteroid_quad0()
{

    ofFill();
    ofEnableAntiAliasing();

    glm::vec2 center = ofGetWindowSize() / 2;

    // make some points radial space
    ofPolyline radial_line;
    ofSetColor(ofColor::black);
    ofSetLineWidth(2.0);
    float radius = 0;
    float radius_max = ofGetWindowHeight() / 2;
    float angle = 0;
    float noise_x = 1;
    float noise_y = 1;
    float noise_z = 1;
    float noise_w = 1;
    //while(radius < radius_max)

    for (int i = 0; i < 10000; i++)
    {
        float x = radius * cos(angle);
        float y = radius * sin(angle);

        ofPoint p;
        p.x = x + center.x;
        p.y = y + center.y;
        p.z = 0;
        radial_line.curveTo(p);
        angle += .1;

        float change_val = 0;
        change_val = gui.extension;
        float noise = 0;
        noise += (gui.micro_noise * .000001);
        noise += (gui.milli_noise * .001);
        noise += (gui.hundo_noise * .01);
        noise += (gui.int_noise * 1);
        noise *= gui.noise_mult;

        change_val += (noise * (ofNoise(p.x, p.y, radius) - .5));
        //change_val += (gui.noise_mult * ofNoise(p.x, p.y, radius) - gui.noise_mult / 2);
        //change_val += (gui.noise_scale * ofNoise(radius, angle) - gui.noise_scale / 2);
        // if(change_val<.025)
        // {
        //     change_val += .025;
        // }
        radius += change_val;
    }

    //ofDrawCircle(center,gui.radius);
    radial_line.draw();
    //draw_perlin_asteroid();
    //draw_asteroid3();
}

void ofApp::draw_asteroid_quad1()
{
    ofNoFill();
    ofEnableAntiAliasing();

    glm::vec2 center = ofGetWindowSize() / 2;

    // make some points radial space
    ofPolyline radial_line;
    ofSetColor(ofColor::black);
    ofSetLineWidth(2.0);

    float radius = 0;
    //float radius_max = gui.radius;
    float angle = 0;
    float noise_x = 1;
    float noise_y = 1;
    float noise_z = 1;
    float noise_w = 1;
    //while(radius < radius_max)

    ofPoint p, last_p;
    last_p = center;

    for (int i = 0; i < 1000; i++)
    {
        float del_x;
        float del_y;

        del_x = gui.noise_mult * (ofNoise(last_p.y) - .5);
        del_y = gui.noise_mult * (ofNoise(last_p.x) - .5);

        p.x = del_x + last_p.x;
        p.y = del_y + last_p.y;
        p.z = 0;
        radial_line.curveTo(p);
        last_p = p;
    }

    radial_line.draw();
}

void ofApp::draw_asteroid_quad2()
{
    //https://forum.openframeworks.cc/t/curves-along-a-path-an-arc/27924/4
    ofSetBackgroundColor(ofColor::white);

    glm::vec2 center = ofGetWindowSize() / 2;

    // make some points radial space
    std::vector<glm::vec3> radial_line;
    ofSetColor(ofColor::black);
    ofSetLineWidth(2.0);

    float radius = gui.radius + .01;
    float radius_max = gui.radius;
    float angle = 0;
    float angle_new = 0;
    float theta = radius / 2;
    for (int i = 0; i < 10000; i++)
    {

        float x = radius * theta * cos(angle);
        float y = radius * theta * sin(angle);

        ofPoint p;
        p.x = x + center.x;
        p.y = y + center.y;
        p.z = 0;

        radial_line.push_back(p);

        angle_new = angle + (radius / (radius * sqrt(1 + (theta * theta))));
        angle = angle_new;
        //std::cout << angle << "," << gui.radius << std::endl;
        //angle+= 1;
        radius += gui.radius;
    }

    float noise = 0;
    noise += (gui.micro_noise * .000001);
    noise += (gui.milli_noise * .001);
    noise += (gui.hundo_noise * .01);
    noise += (gui.int_noise * 1);
    noise *= gui.noise_mult;

    for (int i = 1; i < radial_line.size(); i++)
    {
        ofDrawCircle(radial_line.at(i), 2);
        ofDrawLine(radial_line.at(i), radial_line.at(i - 1));
    }

    // for (int i = 0; i < radial_line.getVertices().size(); i++)
    // {
    //     glm::vec3 v = radial_line.getVertices().at(i);
    //     float change_val = ((ofNoise(v.x, v.y) - .5));

    //     float slope = ((v.y - center.y)/(v.x - center.x));
    //     float del_x = noise * cos(atan(slope));
    //     float del_y = noise * sin(atan(slope));
    //     v.x += del_x;
    //     v.y += del_y;
    //     radial_line.getVertices().at(i) = v;
    // }
}

void ofApp::draw_asteroid_quad3()
{
    ofFill();
    ofEnableAntiAliasing();

    glm::vec2 center = ofGetWindowSize() / 2;

    // make some points radial space
    ofPolyline radial_line;
    ofSetColor(ofColor::black);
    ofSetLineWidth(2.0);
    float radius = 0;
    float radius_max = ofGetWindowHeight() / 2;
    float angle = 0;
    float noise_x = 1;
    float noise_y = 1;
    float noise_z = 1;
    float noise_w = 1;
    //while(radius < radius_max)

    for (int i = 0; i < 1000; i++)
    {
        float x = radius * cos(angle);
        float y = radius * sin(angle);

        ofPoint p;
        p.x = x + center.x;
        p.y = y + center.y;
        p.z = 0;
        radial_line.curveTo(p);
        angle += .1;

        float change_val = 0;
        change_val = gui.extension;
        
        //change_val += (gui.noise_mult * ofNoise(p.x, p.y, radius) - gui.noise_mult / 2);
        //change_val += (gui.noise_scale * ofNoise(radius, angle) - gui.noise_scale / 2);
        // if(change_val<.025)
        // {
        //     change_val += .025;
        // }
        radius += change_val;
    }
    ofPolyline p1 = radial_line.getResampledByCount(500);
    ofPolyline p2 = radial_line.getResampledBySpacing(500);
    //ofDrawCircle(center,gui.radius);
    radial_line.draw();
    
    for (int i = 0 ; i < p1.getVertices().size() ; i++)
    {   
        
        ofPoint point;
        point = p1.getVertices().at(i);
        ofSetColor(ofColor::tomato);
        ofDrawCircle(point,gui.radius);
    }

    for (int i = 0 ; i < p2.getVertices().size() ; i++)
    {   
        
        ofPoint point;
        point = p2.getVertices().at(i);
        ofSetColor(ofColor::steelBlue);
        ofDrawCircle(point,gui.radius);
    }
    //draw_perlin_asteroid();
    //draw_asteroid3();
}

void ofApp::draw()
{
    //https://forum.openframeworks.cc/t/curves-along-a-path-an-arc/27924/4
    ofSetBackgroundColor(ofColor::white);
    gui.draw();

    if (save_to_svg_flag == 1)
    {
        ofBeginSaveScreenAsSVG("test.svg", false, true);
    }

    float H = ofGetWindowHeight();
    float W = ofGetWindowWidth();

    camera_view_box[1].set(0, 0, W / 2, H / 2);
    camera_view_box[0].set(0, H / 2, W / 2, H / 2);
    camera_view_box[2].set(W / 2, 0, W / 2, H / 2);
    camera_view_box[3].set(W / 2, H / 2, W / 2, H / 2);

    ofViewport(camera_view_box[0]);
    draw_asteroid_quad0();
    ofViewport(camera_view_box[1]);
    draw_asteroid_quad1();
    ofViewport(camera_view_box[2]);
    draw_asteroid_quad2();
    ofViewport(camera_view_box[3]);
    draw_asteroid_quad3();

    ofViewport();

    if (save_to_svg_flag)
    {
        ofEndSaveScreenAsSVG();
        save_to_svg_flag = false;
    }

    if (gui.DIAGNOSTICS)
        DisplayDiagnostics();

    gui.GuiDisplay();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch (key)
    {
    case ' ':
        save_to_svg_flag = true;
        break;
    }
}

void ofApp::DisplayDiagnostics()
{
    string fpsStr;
    int base_x = 100, base_y = 100;
    int space = 12;
    int i = 0;

    base_x = ofGetWindowWidth() - 260;
    base_y = ofGetWindowHeight() - 75;
    ofSetColor(0, 0, 0, 255);
    i++;

    //frame rate Statistics
    fpsStr = "frame rate/target: " + ofToString(ofGetFrameRate(), 2) + '/' + ofToString(ofGetTargetFrameRate(), 2);

    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "frame num: " + ofToString(ofGetFrameNum(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "last frame time: " + ofToString(ofGetLastFrameTime(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    //Window Statistics
    fpsStr = "window h/w: " + ofToString(ofGetWindowHeight(), 2) + '/' + ofToString(ofGetWindowWidth(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "window pos x/y: " + ofToString(ofGetWindowPositionX(), 2) + '/' + ofToString(ofGetWindowPositionY(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;
}
