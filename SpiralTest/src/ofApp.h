#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "DisGui.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

class ofApp : public ofBaseApp
{

    void setup(void);
    void update(void);
    void draw();

    //the file for the target doodle, and the directory
    ofFile doodle_file;
    ofDirectory doodle_dir; 

    //for loading the doodles into memory.
    void loaddoodlestomem(std::string filepath);
    std::vector<ofPath> doodle_paths;
    int read_doodle_offset;

    //look in the directory and get a list of the *.bin google files
    void check_doodle_dir_get_filenames(void);
    //void doodle_file_handler(void);

    bool save_to_svg_flag;
    
    void setup_grid(void);
    void draw_paper();

    ofPath readDoodle(unsigned int idx);
    void keyPressed(int key);
    ofRectangle getBoundingBoxOfPath(ofPath &path);

    std::vector<float> rotation_degrees;
    void calc_rotation_values(void);

    void draw_asteroid_quad0();
    void draw_asteroid_quad1();
    void draw_asteroid_quad2();
    void draw_asteroid_quad3();
    

    void draw_perlin_asteroid();
    void draw_asteroid3();
    //instance of the gui
    DisGui gui;
    void DisplayDiagnostics();
    std::vector<ofRectangle> subgrid;

    //for rendering
    ofEasyCam camera[4];
    ofRectangle camera_view_box[4];

};
