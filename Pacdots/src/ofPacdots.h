#ifndef _OFPACDOTS // if this class hasn't been defined, the program can define it
#define _OFPACDOTS // by using this if statement you prevent the class to be called more than once which would confuse the compiler

#include "ofMain.h" // we need to include this to have a reference to the openFrameworks framework

class ofPacdots {

    public: // place public functions or variables declarations here

    // methods, equivalent to specific functions of your class objects
    void setup();    // setup method, use this to setup your object's initial state
    void update();  // update method, used to refresh your objects properties
    void draw();    // draw method, this where you'll do the object's drawing

    // variables
    std::vector<glm::vec3> points;  //the waypoints of the track
    std::vector<double> radii;      //
    double default_radius;          //sets the default radii;
    ofColor color;                  //default color
    glm::vec3 centroid;             //for the center of the points
    bool vibrate;                   //flag to vibrate spheres

    ofPacdots();  // constructor - used to initialize an object, if no properties are passed the program sets them to the default value
    int loadpointsfromcsv(std::string p); //takes a vector of glm::vec3's in x,y,z order and loads them in to the points
    void set_all_radii(float r); //sets all the raddi

    private: // place private functions or variables declarations here

    void set_centroid();
    //points to mesh
    //make triangle mesh from center point

}; // don't forget the semicolon!
#endif