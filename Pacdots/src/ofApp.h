#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxColorMap.h"
#include "ofPacdots.h"

//#include "points.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
    
    ofPacdots dots;
    ofEasyCam camera;
    ofMesh circle;
    ofxPanel gui;

    int phase; 
  };
