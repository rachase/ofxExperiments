#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

     ofSetLogLevel(OF_LOG_VERBOSE);

    dots.loadpointsfromcsv(ofFilePath::getCurrentWorkingDirectory() +"/../src/testpoints.csv");

    ofEnableSmoothing();
    ofSetLineWidth(1);

    ofSetFrameRate(30);
    
    camera.setNearClip(0.1);
    camera.setFarClip(1000000000);
    
    camera.setTarget(dots.centroid);
    camera.setDistance(250);
    
    
}

//---------------------------------------------------------
void ofApp::update()
{
    dots.update();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    ofEnableDepthTest();

    camera.begin();
    dots.draw();
    ofSetColor(ofColor::blue);
    
    camera.end();
    
}