#include "ofPacdots.h"
ofPacdots::ofPacdots(){
    setup();
}

void ofPacdots::setup()
{
    points.clear();
    radii.clear();
    default_radius = 0.5;
    vibrate = true;
    color=ofColor(ofColor::white);
}

void ofPacdots::update()
{
    float d;
    //if vibrate then do that here.    
    if(vibrate)
    {
        for (unsigned int i;i<radii.size();i++)
        {
            d = ofNoise(ofGetElapsedTimeMicros(),
                        radii.at(i));
            d -= 0.5;
            
            //radii.at(i) = ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*.006,radii.at(i)),0,1,default_radius/12,default_radius);
            radii.at(i) += d/5.0;
            //ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*.006,radii.at(i)),0,1,default_radius/12,default_radius);
            if (radii.at(i) <=0)
            {
                 radii.at(i)=0;
            }
            if (radii.at(i) >=default_radius)
            {
                 radii.at(i)=default_radius;
            }
            
            
        }
    }
}

void ofPacdots::set_all_radii(float r)
{
    for(unsigned int i=0; i<radii.size(); i++)
    {
        radii.at(i)=default_radius;
    }
}

void ofPacdots::draw()
{
    ofPushStyle(); //store current style

    //select draw method and then draw

    //default, some dots.
    ofSetColor(ofColor::white);
    for (unsigned int i = 0; i<points.size() ; i++)
    {
        ofDrawSphere(points.at(i),radii.at(i));
    }

    ofPopStyle(); //restore previous style
}

int ofPacdots::loadpointsfromcsv(std::string p)
{
    std::vector < std::string > linesOfTheFile;
    ofBuffer buffer = ofBufferFromFile(p,false);

    //seperate the csv lines by row
    for (auto line : buffer.getLines())
    {
        linesOfTheFile.push_back(line);
    }

    //should have a list of of the rows,
    //check that there are 3, convert to doubles, push to points vec
    std::vector<std::string> d;
    glm::vec3 v;
    points.clear();
    radii.clear();
    for(unsigned int i =0 ; i<linesOfTheFile.size(); i++)
    {
        d.clear();
        d = ofSplitString(linesOfTheFile.at(i),",");
        if (d.size()==3)
        {
            v.x = stod(d.at(0));
            v.y = stod(d.at(1));
            v.z = stod(d.at(2));
            points.push_back(v);  
            radii.push_back(ofMap(ofNoise(v.x*v.y,ofGetElapsedTimeMicros()*100.0),0,1.0,0,default_radius));
        }
        else
        {
            ofLog(OF_LOG_ERROR, "Split string %s converted into size %ld",
                linesOfTheFile.at(i).c_str(),d.size());
        }
    }


    if(linesOfTheFile.size()==points.size())
    {
        ofLog(OF_LOG_VERBOSE, 
            "Read in (%ld) csv lines to (%ld) points",
            linesOfTheFile.size(),points.size());
        set_centroid();
        return 1;
    }
    else
    {
        ofLog(OF_LOG_FATAL_ERROR, 
            "ofPacdots csv lines(%ld) defers from final point size(%ld)",
            linesOfTheFile.size(),points.size());
        
        return 0;
    }
    
}

void ofPacdots::set_centroid()
{
    double x=0,y=0,z=0;

    for(unsigned int i = 0 ; i < points.size() ; i++)
    {
        x+=points.at(i).x;
        y+=points.at(i).y;
        z+=points.at(i).z;
    }

    centroid.x = x/points.size();
    centroid.y = y/points.size();
    centroid.z = z/points.size();
}
