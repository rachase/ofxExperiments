#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxColorMap.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
 
    ofMesh mesh;
    ofEasyCam cam;
    ofLight light;
    ofxColorMap cmap;

    ofxPanel gui;
    ofParameter <ofVec3f> camPosition;
    ofParameter <float> xspacing;
    ofParameter <float> yspacing;
    ofParameter <unsigned int> xsize;
    ofParameter <unsigned int> ysize;
    ofParameter <float> noisespeed;
    ofParameter <float> maxheight;
    ofParameter <float> spheresize;
    ofParameter <float> linewidth;
    ofParameter <unsigned int> startcolor;
    ofParameter <unsigned int> randomizecolor;
    ofParameter <unsigned int> twocolor;
    
    std::string colornames[142]={
        "Spectral",
        "summer",
        "coolwarm",
        "pink_r",
        "Set1",
        "Set2",
        "Set3",
        "brg_r",
        "Dark2",
        "prism",
        "PuOr_r",
        "afmhot_r",
        "terrain_r",
        "PuBuGn_r",
        "RdPu",
        "gist_ncar_r",
        "gist_yarg_r",
        "Dark2_r",
        "YlGnBu",
        "RdYlBu",
        "hot_r",
        "gist_rainbow_r",
        "gist_stern",
        "PuBu_r",
        "cool_r",
        "cool",
        "gray",
        "copper_r",
        "Greens_r",
        "GnBu",
        "gist_ncar",
        "spring_r",
        "gist_rainbow",
        "gist_heat_r",
        "OrRd_r",
        "CMRmap",
        "bone",
        "gist_stern_r",
        "RdYlGn",
        "Pastel2_r",
        "spring",
        "terrain",
        "YlOrRd_r",
        "Set2_r",
        "winter_r",
        "PuBu",
        "RdGy_r",
        "spectral",
        "rainbow",
        "flag_r",
        "jet_r",
        "RdPu_r",
        "gist_yarg",
        "BuGn",
        "Paired_r",
        "hsv_r",
        "bwr",
        "cubehelix",
        "Greens",
        "PRGn",
        "gist_heat",
        "spectral_r",
        "Paired",
        "hsv",
        "Oranges_r",
        "prism_r",
        "Pastel2",
        "Pastel1_r",
        "Pastel1",
        "gray_r",
        "jet",
        "Spectral_r",
        "gnuplot2_r",
        "gist_earth",
        "YlGnBu_r",
        "copper",
        "gist_earth_r",
        "Set3_r",
        "OrRd",
        "gnuplot_r",
        "ocean_r",
        "brg",
        "gnuplot2",
        "PuRd_r",
        "bone_r",
        "BuPu",
        "Oranges",
        "RdYlGn_r",
        "PiYG",
        "CMRmap_r",
        "YlGn",
        "binary_r",
        "gist_gray_r",
        "Accent",
        "BuPu_r",
        "gist_gray",
        "flag",
        "bwr_r",
        "RdBu_r",
        "BrBG",
        "Reds",
        "Set1_r",
        "summer_r",
        "GnBu_r",
        "BrBG_r",
        "Reds_r",
        "RdGy",
        "PuRd",
        "Accent_r",
        "Blues",
        "autumn_r",
        "autumn",
        "cubehelix_r",
        "nipy_spectral_r",
        "ocean",
        "PRGn_r",
        "Greys_r",
        "pink",
        "binary",
        "winter",
        "gnuplot",
        "RdYlBu_r",
        "hot",
        "YlOrBr",
        "coolwarm_r",
        "rainbow_r",
        "Purples_r",
        "PiYG_r",
        "YlGn_r",
        "Blues_r",
        "YlOrBr_r",
        "seismic",
        "Purples",
        "seismic_r",
        "RdBu",
        "Greys",
        "BuGn_r",
        "YlOrRd",
        "PuOr",
        "PuBuGn",
        "nipy_spectral",
        "afmhot"
        };
  };

