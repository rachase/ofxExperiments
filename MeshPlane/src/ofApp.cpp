#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableSmoothing();
    ofSetLineWidth(1);

    mesh.setMode(OF_PRIMITIVE_LINES);
    mesh.enableIndices();
    mesh.enableColors();
    
    ofSetFrameRate(60);
    
    gui.setup();
    gui.add(camPosition.set(
        "camposition",
        ofVec3f(0,0,0),
        ofVec3f(-5000,-5000,-5000),
        ofVec3f(5000,5000,5000)
        )); 
    gui.add(xspacing.set("xspacing",0.1,0,100));
    gui.add(yspacing.set("yspacing",0.1,0,100));
    gui.add(xsize.set("xsize",1,0,100));
    gui.add(ysize.set("ysize",1,0,100));
    gui.add(noisespeed.set("noisespeed",.25,.25,1));
    gui.add(maxheight.set("maxheight",1,1,100));
    gui.add(spheresize.set("spheresize",0.025,0.025,10));
    gui.add(linewidth.set("linewidth",.025,.025,3));
    gui.add(startcolor.set("gradient",0,0,141));
    gui.add(randomizecolor.set("randomize color",0,0,1));
    gui.add(twocolor.set("two color",0,0,1));

    cam.setDistance(15); 
}

//---------------------------------------------------------
void ofApp::update(){
    ofSeedRandom(30);

    mesh.clear();

    for(unsigned int i=0;i<xsize;i++)
    {
        for(unsigned int j=0;j<ysize;j++)
        {
            ofVec3f position = ofVec3f(
                i*xspacing,
                j*yspacing,
                ofMap(ofNoise(ofRandom(1000),ofGetElapsedTimef()*noisespeed),0,1,0,maxheight));
            if(randomizecolor>0)
            {
                mesh.addColor(ofFloatColor(
                    ofNoise(ofRandom(1000),ofGetElapsedTimef()*noisespeed),
                    ofNoise(ofRandom(1000),ofGetElapsedTimef()*noisespeed),
                    ofNoise(ofRandom(1000),ofGetElapsedTimef()*noisespeed)));
            }
            else
            {
                mesh.addColor(cmap.use(ofMap(position.z,0,maxheight,0,255)));
                

            }
            mesh.addVertex(position);
        }
        
    }

    //setup lines along y direction
    for(unsigned int i=0;i<mesh.getNumVertices();i++)
    {
        mesh.addIndex(i);
        mesh.addIndex(i);
        if((i%ysize)==(ysize -1))
        {
            mesh.removeIndex(mesh.getIndices().size()-1);
            mesh.addIndex(i+1);
            i=i+1;
        }
    }

    //remove beginning and end index
    mesh.removeIndex(0);
    mesh.removeIndex(mesh.getIndices().size()-1);

    //setup lines along x direction
    //mesh.addIndex(0);   
    for(unsigned int i=0;i<ysize;i++)
    {
        mesh.addIndex(i);
        for(unsigned int j=1;j<xsize;j++)
        {
            mesh.addIndex(ysize*j+i);
            mesh.addIndex(ysize*j+i);    
        }
        mesh.removeIndex(mesh.getIndices().size()-1);
    }
      
    
    ofSetLineWidth(linewidth);
    
    cam.setTarget(mesh.getCentroid());
   
    cam.setNearClip(0.1);
    cam.setFarClip(1000000000);

    cmap.setMapFromName(colornames[startcolor]);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    ofEnableDepthTest();

    cam.begin();
    for(unsigned int i = 0; i<mesh.getNumVertices();i++)
    {
        ofSetColor(mesh.getColor(i));
        ofDrawSphere(mesh.getVertex(i),spheresize);
    }
    
    mesh.draw();
    
    ofDisableDepthTest();
    cam.end();
    gui.draw();   

    
}