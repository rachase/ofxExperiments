#include "ofMain.h"
#include <boost/circular_buffer.hpp>

class ofTrackDisplay
{

public:
    std::vector<ofPoint> ground_truth;
    std::vector<ofPoint> estimate;

    ofPolyline ground_truth_curve;
    ofPolyline estimate_curve;

    unsigned int track_memory_size;
    int step_size;
    
    ofPoint base_point;

    void draw();
    void update();
    void setup();
    ofTrackDisplay();

};


