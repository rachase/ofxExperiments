//
//  Circulo.cpp
//  circle
//
//  Created by Jacobo Heredia on 5/16/17.
//
//

#define N_LOW -45
#define N_HIGH 45
#define SPHERESIZE 4.5

#include "ofTrackDisplay.h"
ofTrackDisplay::ofTrackDisplay()
{
    track_memory_size = 1000;
    base_point = ofPoint(0,0,0);
    step_size = 12;
    //call this for now. 
    setup();
}

void ofTrackDisplay::setup()
{
    ofPoint p;

    //zero out buffers
    ground_truth.clear();
    estimate.clear();
    ground_truth_curve.clear();
    estimate_curve.clear();

    ///intialzie first index
    ground_truth.push_back(base_point);

    p.x = base_point.x + ofRandom(N_LOW,N_HIGH);
    p.y = base_point.y + ofRandom(N_LOW,N_HIGH);
    p.z = base_point.z + ofRandom(N_LOW,N_HIGH);
    estimate.push_back(p);

    //make the tracks
    for(unsigned int i=1;i<track_memory_size;i++)
    {
        //make some fake ground truth
        p = ground_truth.at(i-1);
        p.x = p.x + step_size + ofRandom(-1,1);
        p.y = p.y + ofRandom(N_LOW,N_HIGH);
        p.z = p.z + ofRandom(N_LOW,N_HIGH);
        ground_truth.push_back(p);

        //noisey it up and call it an estimate
        p.x = p.x + ofRandom(N_LOW,N_HIGH);
        p.y = p.y + ofRandom(N_LOW,N_HIGH);
        p.z = p.z + ofRandom(N_LOW,N_HIGH);
        estimate.push_back(p);
    }

    //setup polyline (need to do this point by point I think)
    for (unsigned int i=0;i<track_memory_size;i++)
    {
        ground_truth_curve.addVertex(ground_truth.at(i));
        estimate_curve.addVertex(estimate.at(i));
    }
}

void ofTrackDisplay::update()
{
    ofPoint p,q;
    //get most recent element (its at bottom of vector)
    p = ground_truth.back();
    //add a new point 
    p.x = p.x + step_size + ofRandom(-1,1);
    p.y = p.y + ofRandom(N_LOW,N_HIGH);
    p.z = p.z + ofRandom(N_LOW,N_HIGH);

    //update ground truth list and polyline
    ground_truth.push_back(p);
    ground_truth.erase(ground_truth.begin());
    ground_truth_curve.addVertex(p);
    ground_truth_curve.removeVertex(0);

    //muddy it up
    p.x = p.x + ofRandom(N_LOW,N_HIGH);
    p.y = p.y + ofRandom(N_LOW,N_HIGH);
    p.z = p.z + ofRandom(N_LOW,N_HIGH);

    //update ground truth list and polyline
    estimate.push_back(p);
    estimate.erase(estimate.begin());
    estimate_curve.addVertex(p);
    estimate_curve.removeVertex(0);


}

void ofTrackDisplay::draw()
{
    ofPoint p;
        
    ofTranslate(-ground_truth.at(0).x,0,0);

    //draw ground_truth (White)
    ofSetColor(0,128,255,255);
    for(unsigned int i=0;i<ground_truth.size();i++)
    {
        p = ground_truth.at(i);
        ofDrawSphere(p.x,p.y,p.z,SPHERESIZE);
    }
    
    //draw estimate
    ofSetColor(0,255,128,255);
    for(unsigned int i=0;i<estimate.size();i++)
    {
        p = estimate.at(i);
        ofDrawSphere(p.x,p.y,p.z,SPHERESIZE);
    }
    
    //draw lines between ground truth and estimate
    ofSetColor(255,255,255,128);
    
    ofDisableLighting();
    for(unsigned int i=0;i<ground_truth.size();i++)
    {
        //ofSetLineWidth(ground_truth.at(i).distance(estimate.at(i)));
        ofDrawLine(ground_truth.at(i),estimate.at(i));
    }

    //draw lines to connect vectors together
    for(unsigned int i=0;i<ground_truth.size()-1;i++)
    {
        ofSetColor(0,128,255,128);
        ofDrawLine(ground_truth.at(i),ground_truth.at(i+1));
        ofSetColor(0,255,128,128);
        ofDrawLine(estimate.at(i),estimate.at(i+1));
    }
    ofEnableLighting();

    //draw polylines
    // ofSetColor(0,0,0,255);
    // ground_truth_curve.draw();
    // ofSetColor(128,128,128,255);
    // estimate_curve.draw();
}
