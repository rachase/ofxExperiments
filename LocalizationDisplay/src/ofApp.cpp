#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    ofSetWindowShape(800,800);
    ofSetFrameRate(20);
    ofEnableDepthTest();
    directionallight.setDirectional();
    
    ofEnableAlphaBlending();
    //ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);

    ofEnableSmoothing(); //smooths out lines
    

    
}

//--------------------------------------------------------------
void ofApp::update(){

    static float x = -M_PI_4;
    x = x + (M_PI/180.0);
    if (x>M_PI_4)x=-M_PI_4;
    lightrotation.set(x,0,.2,1);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    directionallight.enable();
    directionallight.setOrientation(lightrotation);
    cam.begin();
    ofTranslate(-ofGetWindowWidth()/2,0,0);
    testtrack.draw();
    cam.end();
    directionallight.disable();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch (key) {
        case 'q':
            testtrack.update();
            break;
    }
}
