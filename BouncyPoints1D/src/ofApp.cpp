#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableSmoothing();
    ofSetLineWidth(3);

    //init grid
    for (int j = 0 ; j < 20 ; j++)
    {     
       xvals[j]=j*30;
       vel[j]=1;
    }
     
}

//check if there is cross over between points, return decision of what to do
float ofApp::crossover_check(float front, float mid, float back, float * vel)
{
    float retval=mid;
    if(mid<front)
    {
        retval = front;
        *vel = *vel * -1;
    }
    if(mid>back)
    {
        retval = back;
        *vel = *vel * -1;
    }
    
    return retval;
}

void ofApp::squiggle()
{
    static float old_time=0; 
    float new_time = ofGetElapsedTimef();
    
    

    for (int j = 1 ; j < 19 ; j++)
    {
        //xvals[j]+=((new_time-old_time)*vel[j]);
        xvals[j]+=(1*vel[j]);
        crossover_check(xvals[j-1],xvals[j],xvals[j+1],&vel[j]);
    }

    //checks
    for (int j = 0 ; j < 20 ; j++)
    {
        
    }
    
}
//--------------------------------------------------------------
void ofApp::update(){
   
   
   //camera.setGlobalPosition(171.492, -270.95, 218.132);
   //camera.setGlobalOrientation(glm::quat(0.788995, 0.61437, 0.00480374, -0.00374055));
   xvals[0]=10;
   xvals[19]=ofGetWindowWidth()-10;
   

}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    
    squiggle();     
    for (unsigned int i = 0 ; i < 20 ; i ++)
    {
        ofSetColor(colors[i]);
        ofDrawCircle(xvals[i],ofGetWindowHeight()/2,10);
        
    }
}