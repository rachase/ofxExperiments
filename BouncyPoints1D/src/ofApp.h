#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
 
    //user functions
    void squiggle(void);
    float crossover_check(float beg, float mid, float end, float * vel);
 
    //user vars
    float xvals[20];
    float vel[20];

    ofColor colors[20]={
        ofColor::papayaWhip,
        ofColor::plum,
        ofColor::rosyBrown,
        ofColor::seaShell,
        ofColor::sienna,
        ofColor::tomato,
        ofColor::aliceBlue,
        ofColor::aquamarine,
        ofColor::blanchedAlmond,
        ofColor::burlyWood,
        ofColor::navajoWhite,
        ofColor::grey,
        ofColor::hotPink,
        ofColor::honeyDew,
        ofColor::lavenderBlush,
        ofColor::lawnGreen,
        ofColor::lemonChiffon,
        ofColor::mediumOrchid,
        ofColor::oliveDrab,
        ofColor::thistle
        };
    

};