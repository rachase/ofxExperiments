#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableSmoothing();
    ofSetLineWidth(1);

    ofSetFrameRate(30);
    
    gui.setup();


    circle.clear();
    circle.setMode(ofPrimitiveMode::OF_PRIMITIVE_LINE_LOOP);
    for (int i=0;i<360;i++)
    {
        float rad = i*(M_PI/180.0);
        circle.addVertex(glm::vec3(10*cos(rad),10*sin(rad),0));
    }

    camera.setNearClip(0.1);
    camera.setFarClip(1000000000);
    //camera.setTarget(circle.getCentroid());
    camera.setDistance(25);
    
    phase = 0; 
}

//---------------------------------------------------------
void ofApp::update()
{
    int _f;
    for (int i=0;i<circle.getNumVertices(); i++)
    {
        float rad = i*(M_PI/180.0);
        glm::vec3 pos = circle.getVertices().at(i);
        float factor = ofNoise(pos.x,pos.y,std::fmod(ofGetElapsedTimef(),10),ofGetElapsedTimeMicros());
        //circle.getVertices().at(i).x = 10*cos(rad)+cos(rad)*factor;
        //circle.getVertices().at(i).y = 10*sin(rad)+sin(rad)*factor;
        circle.getVertices().at(i).x += cos(rad)*(factor-.5)/25;
        circle.getVertices().at(i).y += sin(rad)*(factor-.5)/25;
        circle.getVertices().at(i).z -= (factor-.5)/25;
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    ofEnableDepthTest();

    camera.begin();
    ofSetColor(ofColor::white);
    circle.drawVertices();
    ofSetColor(ofColor::cyan);
    circle.drawWireframe();
    ofDisableDepthTest();
    camera.end();
    gui.draw();   

    phase++;
}