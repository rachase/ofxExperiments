#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxColorMap.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
    
    ofEasyCam camera;
    ofMesh circle;
    ofxPanel gui;

    int phase; 

  };

