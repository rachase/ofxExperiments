# 3D Grid View

A view of 3D grid.  Variable velocity and line widths.  

<img src="3Dgridview.png" width="70%" height="70%">