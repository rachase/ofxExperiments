#include "ofApp.h"

#define SIZE 15*19
//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableSmoothing();
    ofSetFrameRate(30); 

    gui.setup();
    gui.add(movemethod.set("move method",0,0,1));
    gui.add(velocity.set("velocity",0,0,10));
    gui.add(linewidth.set("line width",0,0,10));
    gui.add(edgewidth.set("edge width",0,0,10));

    initgrid();
    randomize_line_colors();
    make_outer_verts();
   
}

void ofApp::setupsquiggle()
{
    //add some random velocity attributes to the lines
    float checkval;
    for (int i =0 ; i < 20 ; i++)
    {
        for (int j =0 ; j < 9 ; j++)
        {
            checkval=ofRandomf();
            if(checkval>0)
            {
                vel[i][j]=.25;
            }
            else
            {
                vel[i][j]=-.25;
            }
        }
    }
}

void ofApp::make_outer_verts()
{
    //initialize the outer coordiantes of the box
    graph_edge_point[0]=glm::vec3(0,0,0);
    graph_edge_point[1]=glm::vec3(0,0,SIZE);
    graph_edge_point[2]=glm::vec3(0,SIZE,0);
    graph_edge_point[3]=glm::vec3(0,SIZE,SIZE);
    graph_edge_point[4]=glm::vec3(SIZE,0,0);
    graph_edge_point[5]=glm::vec3(SIZE,0,SIZE);
    graph_edge_point[6]=glm::vec3(SIZE,SIZE,0);
    graph_edge_point[7]=glm::vec3(SIZE,SIZE,SIZE);
}

void ofApp::initgrid()
{
    for (int j = 0 ; j < 20 ; j++)
    {     
        //connections are 0-2,1-3,4-5,3-8,7-2,6-5
        gridlines[j][0]=glm::vec3(0,j*15,0);
        gridlines[j][1]=glm::vec3(j*15,0,0);
        gridlines[j][2]=glm::vec3(19*15,j*15,0);
        gridlines[j][3]=glm::vec3(j*15,19*15,0);
        gridlines[j][4]=glm::vec3(0,19*15,j*15);
        gridlines[j][5]=glm::vec3(19*15,19*15,j*15);
        gridlines[j][6]=glm::vec3(19*15,0,j*15);
        gridlines[j][7]=glm::vec3(19*15,j*15,19*15);
        gridlines[j][8]=glm::vec3(j*15,19*15,19*15);
    }
}

void ofApp::randomize_line_colors()
{
    //randomize the line colors
    for (int i =0 ; i < 20 ; i++)
    {
        colors[i].r=ofRandom(255);
        colors[i].g=ofRandom(255);
        colors[i].b=ofRandom(255);
        colors[i].a=255;

    }
}

//check if there is cross over between points, return decision of what to do
//swap velocity directions and also change velocities.  
float ofApp::crossover_check(float front, float mid, float back, float * vel)
{
    float retval=mid;
    float newvel = ofNoise(abs(*vel)*.01);
    float tempfloat;
    if(mid<=front)
    {
        retval = front;
        tempfloat = std::copysignf(newvel,*vel);
        *vel = tempfloat * -1;
    }
    if(mid>=back)
    {
        retval = back;
        tempfloat = std::copysignf(newvel,*vel);
        *vel = tempfloat * -1;
    }
    
    return retval;
}

//lines bounce around
void ofApp::squiggle()
{
    //begining and end point should be the same so the outer 
    //lines are always square
    for (int j = 1 ; j < 19 ; j++)
    {
        //update position
        gridlines[j][0].y+=vel[j][0];
        gridlines[j][1].x+=vel[j][1];    
        gridlines[j][2].y+=vel[j][2];    
        gridlines[j][3].x+=vel[j][3];    
        gridlines[j][4].z+=vel[j][4];    
        gridlines[j][5].z+=vel[j][5];    
        gridlines[j][6].z+=vel[j][6];    
        gridlines[j][7].y+=vel[j][7];    
        gridlines[j][8].x+=vel[j][8];    
    }

    //change velocity and quick check to make sure they dont cross over their neighbors
    for (int j = 1 ; j < 19 ; j++)
    {
        gridlines[j][0].y=crossover_check(gridlines[j-1][0].y,gridlines[j][0].y,gridlines[j+1][0].y,&vel[j][0]);
        gridlines[j][1].x=crossover_check(gridlines[j-1][1].x,gridlines[j][1].x,gridlines[j+1][1].x,&vel[j][1]);
        gridlines[j][2].y=crossover_check(gridlines[j-1][2].y,gridlines[j][2].y,gridlines[j+1][2].y,&vel[j][2]);
        gridlines[j][3].x=crossover_check(gridlines[j-1][3].x,gridlines[j][3].x,gridlines[j+1][3].x,&vel[j][3]);
        gridlines[j][4].z=crossover_check(gridlines[j-1][4].z,gridlines[j][4].z,gridlines[j+1][4].z,&vel[j][4]);
        gridlines[j][5].z=crossover_check(gridlines[j-1][5].z,gridlines[j][5].z,gridlines[j+1][5].z,&vel[j][5]);
        gridlines[j][6].z=crossover_check(gridlines[j-1][6].z,gridlines[j][6].z,gridlines[j+1][6].z,&vel[j][6]);
        gridlines[j][7].y=crossover_check(gridlines[j-1][7].y,gridlines[j][7].y,gridlines[j+1][7].y,&vel[j][7]);
        gridlines[j][8].x=crossover_check(gridlines[j-1][8].x,gridlines[j][8].x,gridlines[j+1][8].x,&vel[j][8]);
    }

}

void ofApp::moveColor(ofColor * c)
{
    ofColor retval;
    float time = ofGetElapsedTimef();
    //retval=ofFloatColor(ofNoise(time*c->r),ofNoise(time*c->g),ofNoise(time*c->b));
    retval=ofFloatColor(ofRandomf(),ofRandomf(),ofRandomf());
    *c=retval;
}

void ofApp::somen(float * val, unsigned int pos)
{
    float q = 19.0*15.0;
    float position_shift = velocity;
    *val+=position_shift;
    if(*val>q)
    {
        *val =fmod(*val,q);
        //moveColor(&colors[pos]);
    }
}

//lines bounce around
void ofApp::move()
{
    float q = 19.0*15.0;
    
    //begining and end point should be the same so the outer 
    //lines are always square
    for (int j = 0 ; j < 19 ; j++)
    {
        //update position
        somen(&gridlines[j][0].y,j);
        somen(&gridlines[j][1].x,j);
        somen(&gridlines[j][2].y,j);
        somen(&gridlines[j][3].x,j);
        somen(&gridlines[j][4].z,j);
        somen(&gridlines[j][5].z,j);
        somen(&gridlines[j][6].z,j);
        somen(&gridlines[j][7].y,j);
        somen(&gridlines[j][8].x,j);
        
    }

    
}

//--------------------------------------------------------------
void ofApp::update()
{
    if (movemethod!=movemethod_previous)
    {
        movemethod_previous=movemethod;
        switch(movemethod)
        {
            //move
            case 0: initgrid();
                    make_outer_verts();
                    randomize_line_colors();
            break;
            // squiggle
            case 1: initgrid();
                    make_outer_verts();
                    randomize_line_colors();
                    setupsquiggle();
            break;

            default: move();
            break;
        }
        
    }
   camera.setGlobalPosition(171.492, -270.95, 218.132);
   camera.setGlobalOrientation(glm::quat(0.788995, 0.61437, 0.00480374, -0.00374055));
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::black);
    ofSetColor(ofColor::white);
    switch(movemethod)
    {
        //move
        case 0: move();
        break;
        // squiggle
        case 1: squiggle();
        break;

        default: 
        break;
    }

    gui.draw();
    camera.begin();
    
    for (unsigned int i = 0; i < 20 ; i ++)
    {
        //connections are 0-2,1-3,4-5,3-8,7-2,6-5
        ofSetColor(colors[i]);
        ofSetLineWidth(linewidth);
        ofDrawLine(gridlines[i][0],gridlines[i][2]);
        ofDrawLine(gridlines[i][1],gridlines[i][3]);
        ofDrawLine(gridlines[i][4],gridlines[i][5]);
        ofDrawLine(gridlines[i][3],gridlines[i][8]);
        ofDrawLine(gridlines[i][7],gridlines[i][2]);
        ofDrawLine(gridlines[i][6],gridlines[i][5]);
    }

    ofSetColor(ofColor::white);
    ofSetLineWidth(edgewidth);
    ofDrawLine(graph_edge_point[0],graph_edge_point[2]);
    ofDrawLine(graph_edge_point[2],graph_edge_point[3]);
    ofDrawLine(graph_edge_point[0],graph_edge_point[4]);
    ofDrawLine(graph_edge_point[4],graph_edge_point[5]);
    ofDrawLine(graph_edge_point[2],graph_edge_point[6]);
    ofDrawLine(graph_edge_point[4],graph_edge_point[6]);
    ofDrawLine(graph_edge_point[3],graph_edge_point[7]);
    ofDrawLine(graph_edge_point[5],graph_edge_point[7]);
    ofDrawLine(graph_edge_point[6],graph_edge_point[7]);

    ofDrawSphere(glm::vec3(-1,-1,0),1);
    for (int i =0; i<9;i++)
    {
        edge_points[i] = camera.worldToScreen(graph_edge_point[i]);
    }
    
    camera.end();

    for (int i =0; i<9;i++)
    {
    std::string s = std::to_string(edge_points[i].x) + " , " + std::to_string(edge_points[i].y);
  	ofDrawBitmapString(s,edge_points[i].x,edge_points[i].y);
    }




}