#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

    void setup(void);
    void update(void);
    void draw();
 
    //helper functions
    void moveColor(ofColor * c);
    void somen(float * val, unsigned int pos);
    void setupsquiggle(void);
    void initgrid(void);
    void randomize_line_colors(void);
    void make_outer_verts(void);


    //user functions
    void squiggle(void);
    void move(void);
    void set_bounce_velocities(void);
    float crossover_check(float front, float mid, float back, float * vel);
 
    //user vars
    glm::vec3 gridlines[20][9];
    glm::vec3 graph_edge_point[9];
    glm::vec3 edge_points[9];
    float vel[20][9];
    ofEasyCam camera;

    ofColor colors[20];
    
    ofxPanel gui;
    ofParameter <unsigned int> movemethod;
    unsigned int movemethod_previous;
    ofParameter <float> velocity;
    ofParameter <float> linewidth;
    ofParameter <float> edgewidth;
    
};